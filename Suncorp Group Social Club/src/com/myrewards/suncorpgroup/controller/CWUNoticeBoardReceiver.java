package com.myrewards.suncorpgroup.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;

import com.myrewards.suncorpgroup.model.NoticeId;
import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.service.GrabItNowService;
import com.myrewards.suncorpgroup.utils.DatabaseHelper;
import com.myrewards.suncorpgroup.utils.Utility;

@SuppressWarnings("deprecation")
public class CWUNoticeBoardReceiver extends BroadcastReceiver implements CWUServiceListener{
	ArrayList<NoticeId> noticeid;
	DatabaseHelper dbHelper;
	Context context;
	String noticeMessages = "";

	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;
		dbHelper = new DatabaseHelper(context);
		ConnectivityManager NoticeCM = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (Utility.isOnline(NoticeCM)) {
			GrabItNowService.getGrabItNowService()
					.sendNoticeBoardCountIdRequset(this);
		} else {
			Utility.showMessage(context, "No Internet Connection");
		}

	}

	private void sendNotification() {
		final NotificationManager manager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		final Notification msg = new Notification(R.drawable.my_notice,
				"Noticeboard Updates !!!", System.currentTimeMillis());
		msg.sound = RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		
		Intent in = new Intent(context, DashboardScreenActivity.class);

		PendingIntent pin = PendingIntent.getActivity(context, 0, in, 0);
		msg.setLatestEventInfo(context, "Noticeboard Updates !!!",
				"New message from noticeboard.", pin);
		msg.flags = Notification.FLAG_AUTO_CANCEL;
		manager.notify(1, msg);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {

			if (eventType == 16) {
				if (response != null) {
					noticeid = (ArrayList<NoticeId>) response;
					int i = -1;
					if (noticeid != null) {
						List<String> noticeIDsList = new ArrayList<String>();
						for (int n = 0; n < noticeid.size(); n++) {
							if (!(noticeid.get(n).getNoticeDetails() == null))
								noticeIDsList.add(noticeid.get(n)
										.getNoticeDetails());
						}
						if (noticeIDsList.size() != dbHelper.getExistingIDs()
								.size()) {
							for (int m = 0; m < dbHelper.getExistingIDs()
									.size(); m++) {
								if (noticeIDsList.contains(dbHelper
										.getExistingNoticeRealIDs().get(m))) {

								} else {
									dbHelper.deleteNoticeIdDetails(dbHelper
											.getExistingNoticeRealIDs().get(m));
								}
							}
							if (noticeIDsList.size() > dbHelper
									.getExistingIDs().size()) {
								sendNotification();
							}
						}

						for (int k = 0; k < noticeid.size(); k++) {
							if (noticeid.get(k).getNoticeDetails() != null) {
								i++;
								List<String> list = dbHelper.getExistingIDs();
								if (!list.contains(noticeid.get(k)
										.getNoticeDetails())) {
									dbHelper.addnoticeiddetails(noticeid.get(k)
											.getNoticeDetails());
								}
							}
						}
						Log.v("Hari--->", "Notice List Count = " + i);
					}
				}
			}
		} catch (Exception e) {
		
			Toast.makeText(context, "Server busy. Please wait or try again.",
					Toast.LENGTH_LONG).show();
			if (e != null) {
				e.printStackTrace();
			}
		}
	}

}
