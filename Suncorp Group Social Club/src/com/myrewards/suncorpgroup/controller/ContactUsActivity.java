package com.myrewards.suncorpgroup.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.suncorpgroup.utils.Utility;

@SuppressWarnings("deprecation")
public class ContactUsActivity extends Activity implements OnClickListener {
	Button mailBtn, callBtn, backBtn, scanBarBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_us_screen);
		
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		
		TextView titleTV=(TextView)findViewById(R.id.titleTVID);
		titleTV.setText(getResources().getString(R.string.contact_us_title));
		titleTV.setTypeface(Utility.font_bold);
		scanBarBtn=(Button)findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);	
		
		backBtn=(Button)findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		
		mailBtn=(Button)findViewById(R.id.contactUsMailBtnID);
		mailBtn.getLayoutParams().width = (int) (Utility.screenWidth / 2.2); //218
		mailBtn.getLayoutParams().height = (int) (Utility.screenHeight / 3.66);
		
		callBtn=(Button)findViewById(R.id.contactUsCallBtnID);
		callBtn.getLayoutParams().width = (int) (Utility.screenWidth / 2.2);
		callBtn.getLayoutParams().height = (int) (Utility.screenHeight / 3.66);
		
		backBtn.setOnClickListener(this);
		mailBtn.setOnClickListener(this);
		callBtn.setOnClickListener(this);;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backBtnID:
			try {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				ContactUsActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				ContactUsActivity.this.finish();
			}
			break;
		case R.id.contactUsMailBtnID:
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				String toClientemail = getResources().getString(R.string.send_email_hint);
				Intent email = new Intent(Intent.ACTION_SEND);
				email.putExtra(Intent.EXTRA_EMAIL, new String[] { toClientemail });
				email.putExtra(Intent.EXTRA_SUBJECT, "Contact Us");
				email.putExtra(Intent.EXTRA_TEXT, "");

				// need this to prompts email client only
				email.setType("message/rfc822");
				startActivity(Intent.createChooser(email, "Choose an Email client :"));
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
				//layout.getBackground().setAlpha(128);  // 50% transparent
				 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
				//showDialog(NO_NETWORK_CON);
			}
			break;
		case R.id.contactUsCallBtnID:
			showDialog(1207);
			break;

		default:
			break;
		}
	}
	
	protected Dialog onCreateDialog(int id) {
		if (id == 1207) {
			AlertDialog deleteFavAlert = null;
				LayoutInflater liDelete = LayoutInflater.from(this);
				View deleteFavView = liDelete.inflate(R.layout.dialog_layout_delete_favorite, null);
				AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(this);
				adbDeleteFav.setView(deleteFavView);
				deleteFavAlert = adbDeleteFav.create();
				deleteFavAlert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				deleteFavAlert.show();
			return deleteFavAlert;
		}
		return super.onCreateDialog(id);
	}

	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1207) {
				final AlertDialog alt3 = (AlertDialog) dialog;
				TextView alertTilteTV = (TextView) alt3.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTilteTV.setTypeface(Utility.font_bold);
				alertTilteTV.setText("Call !");
				TextView tv22 = (TextView) alt3.findViewById(R.id.deleteFavTVID);
				tv22.setTypeface(Utility.font_reg);
				final String contactUsNumber = "+61731354743";
				tv22.setText(" "+contactUsNumber+" ");
				Button deleteFavYesBtn = (Button) alt3.findViewById(R.id.delete_fav_yesBtnID);
				deleteFavYesBtn.setTypeface(Utility.font_bold);
				deleteFavYesBtn.setText("Call");
				Button deleteNoFavBtn = (Button) alt3.findViewById(R.id.delete_fav_noBtnID);
				deleteNoFavBtn.setTypeface(Utility.font_bold);
				deleteNoFavBtn.setText("Cancel");
				deleteFavYesBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							Intent intent = new Intent(Intent.ACTION_CALL);
							intent.setData(Uri.parse("tel: " + contactUsNumber));
							startActivity(intent);	
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
							}
						
						}
						alt3.dismiss();
					}
				});
				deleteNoFavBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						alt3.dismiss();
					}
				});
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			try {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				ContactUsActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				ContactUsActivity.this.finish();
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}
