package com.myrewards.suncorpgroup.controller;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.suncorpgroup.cache.SmartImageView;
import com.myrewards.suncorpgroup.model.Product;
import com.myrewards.suncorpgroup.model.ProductDetails;
import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.service.GrabItNowService;
import com.myrewards.suncorpgroup.utils.ApplicationConstants;
import com.myrewards.suncorpgroup.utils.Utility;

public class DailyDealsActivity extends BaseActivity implements
		CWUServiceListener {
	Product product;
	List<ProductDetails> productList;
	public static View loading;
	public static SmartImageView dailyDealsIV;
	public static TextView noDailyDealsTV;
	ImageView bannerIV;
	String imageURL;
	@SuppressWarnings("unused")
	private static int countpos;

	@SuppressLint("ShowToast")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.daily_deals_main);
		setHeaderTitle(getResources().getString(R.string.daily_deals_text));

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		
		noDailyDealsTV = (TextView) findViewById(R.id.dailyDealsLoadTVID);
		noDailyDealsTV.setTypeface(Utility.font_bold);

		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			GrabItNowService.getGrabItNowService().sendDailyDealsRequest(this);
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			DailyDealsActivity.this.finish();
		}
		dailyDealsIV = (SmartImageView) findViewById(R.id.dailyDealsIVID);
		dailyDealsIV.setEnabled(false);
		dailyDealsIV.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent detailsIntent = new Intent(DailyDealsActivity.this, ProductDetailsActivity.class);
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY, product.getId());
					detailsIntent.putExtra(ApplicationConstants.IMAGE_URL_KEY, imageURL);
					detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY, 0);
					detailsIntent.putExtra(ApplicationConstants.DAILYDEALSIMAGE, "dailyDealsPic");
					startActivity(detailsIntent);
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			}
		});
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (response != null) {
				if (response instanceof String) {
					Utility.showMessage(this, response.toString());
				} else {
					if (eventType == 13) {
						product = (Product) response;
						productList = new ArrayList<ProductDetails>();
						imageURL = ApplicationConstants.DAILY_DEALS_IMAGE_WRAPPER
								+ product.getId() + "."
								+ product.getHotoffer_extension();
						
						String dailyDeals = "DailyDealsImage";
						loading.setVisibility(View.VISIBLE);
						Log.w("Hari-->", imageURL);
						try {
							newCardImagesLoading(imageURL, dailyDeals);
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				Log.v("Hari---->", e.getMessage());
				Toast.makeText(getApplicationContext(), "Server busy. Please wait or try again", Toast.LENGTH_LONG).show();
			}
		}
	}
	
	private void newCardImagesLoading(String cardURL, String _dailyDeals) {
		// Image url
        String image_url = cardURL;
        Log.w("Hari-->", cardURL);
        
        // ImageLoader class instance
   //    MyImageLoader imgLoader = new MyImageLoader(getApplicationContext(), _dailyDeals);
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
       try {
    	  // imgLoader.DisplayImage(image_url, dailyDealsIV);
    	   dailyDealsIV.setImageUrl(image_url);
    	   dailyDealsIV.setEnabled(true);
    	   loading.setVisibility(View.GONE);
    	   //dailyDealsIV.setEnabled(true);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				e.printStackTrace();
			}
		}
	}

/*	@Override
	public void onImageLoadComplete(Bitmap bitmap) {
		try {
			Drawable d = new BitmapDrawable(getResources(), bitmap);
			dailyDealsIV.setBackgroundDrawable(d);
			loading.setVisibility(View.GONE);
			dailyDealsIV.setEnabled(true);
		} catch (Exception e) {
			e.printStackTrace();
			Log.v("Hari---->", e.getMessage());
		}
	}

	@Override
	public void onImageLoadFailed(String msg) {

	}*/
}
