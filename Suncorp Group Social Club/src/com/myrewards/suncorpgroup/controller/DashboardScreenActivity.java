package com.myrewards.suncorpgroup.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.suncorpgroup.utils.DatabaseHelper;
import com.myrewards.suncorpgroup.utils.Utility;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;
import com.readystatesoftware.viewbadger.BadgeView;

/**
 * 
 * @author HARI This class is used to display like menu
 */

@SuppressLint("ShowToast")
public class DashboardScreenActivity extends Activity implements
		OnClickListener {

	// custom dialog fields
	public TextView logoutText, alertTitleTV;
	public Button loginbutton1;
	public Button cancelbutton1;
	final private static int LOGOUT = 1;
	public ImageView lalertImg;
	public TextView textLog;
	ImageView noticeboardBadge;
	DatabaseHelper helper;
	static int noticeCount = 0;
	public ImageView noticeboardunread;
	public static boolean noticeboardcount = false;
	boolean doubleBackToExitPressedOnce = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard1);
		// setDashboard();

		getWindow().setSoftInputMode(
			    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
			);
		
		// Push Notifications Adding...................
		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {

try
{
			Parse.initialize(this,"JSrtHvbPWXyCx9jtPGlv1lvgLAn7G18vry8NhWsD", "N7Sno5wWSAnAYOfIZUyW1KjHLte294bnnsPVFFRN");

			  PushService.setDefaultPushCallback(this, AppPushNotificationActivity.class);
			  ParseInstallation.getCurrentInstallation().saveInBackground();
			  ParseAnalytics.trackAppOpened(getIntent());
}
catch(Exception e)
{
	if(e!=null)
	{
		e.printStackTrace();
	}
}
		

		} else {
			// The Custom Toast Layout Imported here

			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));
			// layout.getBackground().setAlpha(128); // 50% transparent

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_SHORT);
			toast.setView(layout);
			toast.show();
		}

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerIVID);
		headerImage.getLayoutParams().height = Utility.screenHeight / 10;

		helper = new DatabaseHelper(DashboardScreenActivity.this);

		RelativeLayout myUnionRL = (RelativeLayout) findViewById(R.id.myUnionRLID);
		myUnionRL.setOnClickListener(this);
		RelativeLayout myDelegatesRL = (RelativeLayout) findViewById(R.id.myDelegatesRLID);
		myDelegatesRL.setOnClickListener(this);
		RelativeLayout myCardRL = (RelativeLayout) findViewById(R.id.myCardRLID);
		myCardRL.setOnClickListener(this);
		RelativeLayout myUnionContactsRL = (RelativeLayout) findViewById(R.id.myUnionContactsRLID);
		myUnionContactsRL.setOnClickListener(this);
		RelativeLayout myNoticeBoardRL = (RelativeLayout) findViewById(R.id.myNoticeBoardRLID);
		myNoticeBoardRL.setOnClickListener(this);
		RelativeLayout sendAFriendRL = (RelativeLayout) findViewById(R.id.sendAFriendRLID);
		sendAFriendRL.setOnClickListener(this);
		RelativeLayout mySpecialOffersRL = (RelativeLayout) findViewById(R.id.mySpecialOffersRLID);
		mySpecialOffersRL.setOnClickListener(this);
		RelativeLayout searchMyRewardsRL = (RelativeLayout) findViewById(R.id.searchMyRewardsRLID);
		searchMyRewardsRL.setOnClickListener(this);

		RelativeLayout lagoutRL = (RelativeLayout) findViewById(R.id.lagoutRLID);
		lagoutRL.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// showDialog(LOGOUT);
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentMyAccount = new Intent(
							DashboardScreenActivity.this,
							MyAccountActivity.class);
					intentMyAccount.putExtra(Utility.DASHBOARD_ICON_ID,
							Utility.MY_ACCOUNT);
					startActivity(intentMyAccount);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here

					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));
					// layout.getBackground().setAlpha(128); // 50% transparent

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			}
		});

		RelativeLayout myParkingTimeRL = (RelativeLayout) findViewById(R.id.myParkingTimeRLID);
		myParkingTimeRL.setOnClickListener(this);
		RelativeLayout myissueRL = (RelativeLayout) findViewById(R.id.myissueRLID);
		myissueRL.setOnClickListener(this);
		RelativeLayout myAccountRL = (RelativeLayout) findViewById(R.id.myAccountRLID);
		myAccountRL.setOnClickListener(this);

		noticeboardunread = (ImageView) findViewById(R.id.noticeBadgeID);
		BadgeView badge = new BadgeView(DashboardScreenActivity.this,
				noticeboardunread);
		try {
			if (helper.getNoticeDetails() != 0) {
				badge.setText(Integer.toString(helper.getNoticeDetails()));
				badge.setBadgeBackgroundColor(Color.parseColor("#A8AE00"));
				badge.setBadgePosition(BadgeView.POSITION_BOTTOM_LEFT);
				badge.show();
			} else {
				badge.setVisibility(View.GONE);
				noticeboardunread.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
		}
	}

	private void setParsePushes() {
		try {

			String android_id = Secure.getString(getApplicationContext()
					.getContentResolver(), Secure.ANDROID_ID);
			Log.e("LOG", "android id >>" + android_id);
			// set parse initializer
			Parse.initialize(getApplicationContext(),
					"rWPTDGxuh0LSGwtLh6Fw0qrFisGQUBU7QJ31DlHH",
					"noAKYyY1h8gprKNfM3GTN1tKhN0NL6d9BNTFV7tE");
			ParseAnalytics.trackAppOpened(getIntent());
			// PushService.setDefaultPushCallback(this,
			// AppPushNotificationActivity.class);
			// ParseInstallation.getCurrentInstallation().saveInBackground();

			PushService.setDefaultPushCallback(getApplicationContext(),
					AppPushNotificationActivity.class);

			ParseInstallation installation = ParseInstallation
					.getCurrentInstallation();
			installation.put("UniqueId", android_id);
			Log.e("LOG", "android id >>" + android_id);
			installation.saveInBackground();
			ParseUser.enableAutomaticUser();
			ParseACL defaultACL = new ParseACL();
			// Optionally enable public read access.
			// defaultACL.setPublicReadAccess(true);
			ParseACL.setDefaultACL(defaultACL, true);
		} catch (NullPointerException e) {
			if (e != null) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
		}

	}

	// to create custom alerts
	protected Dialog onCreateDialog(int id) {
		AlertDialog dialogDetails = null;
		switch (id) {
		case LOGOUT:
			LayoutInflater inflater = LayoutInflater.from(this);
			View dialogview = inflater.inflate(R.layout.dialog_layout_logout,
					null);
			AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
			dialogbuilder.setView(dialogview);
			dialogDetails = dialogbuilder.create();
			dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
			dialogDetails.show();
			break;
		}
		return dialogDetails;
	}

	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
			switch (id) {
			case LOGOUT:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView) alertDialog1
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				logoutText = (TextView) alertDialog1
						.findViewById(R.id.my_logoutTVID);
				logoutText.setTypeface(Utility.font_reg);
				loginbutton1 = (Button) alertDialog1
						.findViewById(R.id.loginBtnH_ID);
				loginbutton1.setTypeface(Utility.font_bold);
				cancelbutton1 = (Button) alertDialog1
						.findViewById(R.id.btn_cancelH_ID);
				cancelbutton1.setTypeface(Utility.font_bold);

				loginbutton1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						try {
							helper.deleteLoginDetails();
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
							}
						}
						Intent logoutIntent = new Intent(
								DashboardScreenActivity.this,
								LoginScreenActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP);
						startActivity(logoutIntent);
						LoginScreenActivity.etUserId.setText("");
						LoginScreenActivity.etPasswd.setText("");
						DashboardScreenActivity.this.finish();
						alertDialog1.dismiss();
					}
				});
				cancelbutton1.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.myUnionRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					String url = "http://www.suncorpsocialclub.com/";
					Intent viewIntent = new Intent(
							"android.intent.action.VIEW", Uri.parse(url));
					startActivity(viewIntent);

				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.myDelegatesRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					String url = "https://www.facebook.com/SuncorpGroupSocialClub";
					Intent viewIntent = new Intent(
							"android.intent.action.VIEW", Uri.parse(url));
					startActivity(viewIntent);

				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.myCardRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentMyNoticeBoard = new Intent(
							DashboardScreenActivity.this,
							MyNoticeBoardActivity.class);
					intentMyNoticeBoard.putExtra(Utility.DASHBOARD_ICON_ID,
							Utility.MY_NOTICE_BOARD);
					intentMyNoticeBoard.putExtra(Utility.readNoticesEXTRA,
							"READ_HEADER");
					startActivity(intentMyNoticeBoard);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.myUnionContactsRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent events = new Intent(DashboardScreenActivity.this,
							FutureEventsActivity.class);
					events.putExtra(Utility.DASHBOARD_ICON_ID,
							Utility.MY_UNION_CONTACTS);
					startActivity(events);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.myNoticeBoardRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentMyUnionContacts = new Intent(
							DashboardScreenActivity.this,
							ContactUsActivity.class);
					intentMyUnionContacts.putExtra(Utility.DASHBOARD_ICON_ID,
							Utility.MY_UNION_CONTACTS);
					startActivity(intentMyUnionContacts);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.sendAFriendRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentSendaFriend = new Intent(
							DashboardScreenActivity.this, MyCardActivity.class);
					intentSendaFriend.putExtra(Utility.DASHBOARD_ICON_ID,
							Utility.MY_CARD);
					startActivity(intentSendaFriend);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.mySpecialOffersRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentGIN1 = new Intent(
							DashboardScreenActivity.this,
							GrabitNowTabsActivity.class);
					intentGIN1.putExtra(Utility.DASHBOARD_ICON_ID, 4);
					startActivity(intentGIN1);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.searchMyRewardsRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentGIN2 = new Intent(
							DashboardScreenActivity.this,
							GrabitNowTabsActivity.class);
					intentGIN2.putExtra(Utility.DASHBOARD_ICON_ID, 2);
					startActivity(intentGIN2);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.myParkingTimeRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				/*	String path = "http://www.perksatwork.com.au/?user="
							+ Utility.user.getUsername() + "&pageid=1";*/
					String path="http://myrewards.suncorpsocialclub.com.au/products/view/1031337";
					Intent viewIntent = new Intent(
							"android.intent.action.VIEW", Uri.parse(path));
					startActivity(viewIntent);

				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.myissueRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentMyAccount = new Intent(
							DashboardScreenActivity.this,
							NewParkingTimeActivity.class);
					intentMyAccount.putExtra(Utility.DASHBOARD_ICON_ID,
							Utility.MY_PARKING_TIME);
					startActivity(intentMyAccount);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		case R.id.myAccountRLID:
			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentMyIssue = new Intent(
							DashboardScreenActivity.this,
							GrabitNowTabsActivity.class);
					intentMyIssue.putExtra(Utility.DASHBOARD_ICON_ID, 1);
					startActivity(intentMyIssue);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_SHORT);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
				}
			}
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (doubleBackToExitPressedOnce) {
				DashboardScreenActivity.this.finish();
				return doubleBackToExitPressedOnce;
			}
			doubleBackToExitPressedOnce = true;
			Toast.makeText(DashboardScreenActivity.this,
					"Press again to minimize the app.", 400).show();
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2000);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}