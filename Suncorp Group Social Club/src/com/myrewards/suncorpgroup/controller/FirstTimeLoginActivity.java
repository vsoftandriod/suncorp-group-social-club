package com.myrewards.suncorpgroup.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.service.GrabItNowService;
import com.myrewards.suncorpgroup.utils.Utility;
import com.myrewards.suncorpgroup.xml.FirsttimeLoginParser;

/**
 * 
 * @author HARI
 * 
 *         This class is used to register when user is login for first time
 */

@SuppressWarnings("deprecation")
@SuppressLint("ShowToast")
public class FirstTimeLoginActivity extends Activity implements
		android.view.View.OnClickListener, OnItemSelectedListener,
		CWUServiceListener, OnLongClickListener {
	private EditText firstNameEt, lastNameEt, passwdEt, confpasswdEt, emailEt,
			referalNumberET, tandcFLED;
	protected CheckBox chBox1, chBox2;
	private Button submitBtn1, closebtn;
	private Spinner stateSP2, countrySP1;
	public static String s1, s2;
	public static int id = 0;
	int arg2;
	public Animation movement5;
	public LinearLayout firstTimeLoginLL;
	final private static int FIELDS_ERROR = 1;
	final private static int SUCCESS_REG = 2;
	final private static int REQUIRED_PASSWORD = 3;
	final private static int DIALOG_SEND_INVALID_EMAIL = 4;
	final private static int FAILED_REG = 5;
	final private static int SELECT_COUNTRY = 6;
	final private static int SELECT_AGREE = 7;
	Button okbutton;
	protected TextView allFieldsTV, successTV, passwdTV, textTV, firstTitleTV,
			checkTextTV1, checkTextTV2, alertTilteTv, alertMsgTV;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first_time_login);
		firstTimeLoginLL = (LinearLayout) findViewById(R.id.firstTimeLoginLLID);

		firstNameEt = (EditText) findViewById(R.id.firstNameETID);
		firstNameEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		firstNameEt.setTypeface(Utility.font_reg);
		lastNameEt = (EditText) findViewById(R.id.lastNameETID);
		lastNameEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		lastNameEt.setTypeface(Utility.font_reg);
		passwdEt = (EditText) findViewById(R.id.passwdETID);
		passwdEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		passwdEt.setTypeface(Utility.font_reg);
		confpasswdEt = (EditText) findViewById(R.id.confPasswdETID);
		confpasswdEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		confpasswdEt.setTypeface(Utility.font_reg);
		emailEt = (EditText) findViewById(R.id.emailETID);
		emailEt.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		emailEt.setTypeface(Utility.font_reg);
		referalNumberET = (EditText) findViewById(R.id.referalNumberETID);
		referalNumberET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		referalNumberET.setTypeface(Utility.font_reg);
		tandcFLED = (EditText) findViewById(R.id.tandcFLEDID);
		tandcFLED.getLayoutParams().height = (int) (Utility.screenHeight / 4);
		tandcFLED.setTypeface(Utility.font_reg);

		firstNameEt.setOnLongClickListener(this);
		lastNameEt.setOnLongClickListener(this);
		passwdEt.setOnLongClickListener(this);
		confpasswdEt.setOnLongClickListener(this);
		emailEt.setOnLongClickListener(this);

		movement5 = AnimationUtils.loadAnimation(this, R.anim.animation_test5);

		// You can now apply the animation to a view
		firstTimeLoginLL.startAnimation(movement5);

		submitBtn1 = (Button) findViewById(R.id.submitBtnID);
		submitBtn1.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		submitBtn1.setOnClickListener(this);

		closebtn = (Button) findViewById(R.id.closeftlBtnID);
		closebtn.setOnClickListener(this);

		chBox1 = (CheckBox) findViewById(R.id.checKBox_id1);
		chBox2 = (CheckBox) findViewById(R.id.checKBox_id2);

		// select country
		countrySP1 = (Spinner) findViewById(R.id.countrySpinnerID);
		countrySP1.setOnItemSelectedListener(this);

		// select state
		stateSP2 = (Spinner) findViewById(R.id.stateSpinnerID);
		stateSP2.setOnItemSelectedListener(this);

		stateSP2.setVisibility(View.GONE);
		ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(
				this, R.array.category_countries,
				android.R.layout.simple_spinner_item);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		countrySP1.setAdapter(adapter1);

		countrySP1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				ArrayAdapter<CharSequence> adapter2 = null;
				if (arg2 == 0) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.selectstatearray,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 1) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_australia,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}

				} else if (arg2 == 2) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_hongkong,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 3) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_india,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 4) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_newzealand,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 5) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_philippines,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 6) {
					adapter2 = ArrayAdapter.createFromResource(
							FirstTimeLoginActivity.this,
							R.array.category_state_singapore,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.submitBtnID) {
			String stateSPITEMS[][] = {
					{ "" },
					{ "", "ACT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA" },
					{ "HK" },
					{ "", "AP", "DL", "GJ", "HR", "KA", "MH", "TN", "UP", "WB" },
					{ "", "AUK", "BOP", "CAN", "FIL", "GIS", "HKB", "MBH",
							"MWT", "NAB", "NTL", "OTA", "STL", "TKI", "TMR",
							"TSM", "WGI", "WGN", "WKO", "WPP", "WTC" },
					{ "", "LUZ", "MIN", "NCR", "VIS" },
					{ "", "CS", "NES", "NWS", "SES", "SWS" } };

			String mFirstName = firstNameEt.getText().toString();
			String mLastName = lastNameEt.getText().toString();
			String mPasswd = passwdEt.getText().toString();
			String mConPasswd = confpasswdEt.getText().toString();
			String mEmail = emailEt.getText().toString();
			String mCountrySP = countrySP1.getSelectedItem().toString();
			String mStateSP = stateSP2.getSelectedItem().toString();

			int countrySPPOS = countrySP1.getSelectedItemPosition();
			int stateSPPOS = stateSP2.getSelectedItemPosition();

			int newslatter;
			if (chBox2.isChecked()) {
				newslatter = 1;
			} else {
				newslatter = 0;
			}

			if (mFirstName.length() > 0 && mLastName.length() > 0
					&& mPasswd.length() > 0 && mEmail.length() > 0
					&& mCountrySP.length() > 0 && mStateSP.length() > 0
					&& FirsttimeLoginParser.first_login_id != 0) {
				if (mPasswd.equals(mConPasswd)) {
					if (isEmailValid(mEmail)) {
						if (!(mCountrySP.equals("Select Country"))
								&& !(mStateSP.equals("Select State"))) {
							if (chBox1.isChecked() == true) {
								if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
									GrabItNowService.getGrabItNowService()
									.sendFirstTimeDetails(
											FirstTimeLoginActivity.this,
											mFirstName,
											mLastName,
											mPasswd,
											mEmail,
											mCountrySP,
											stateSPITEMS[countrySPPOS][stateSPPOS],
											newslatter,
											FirsttimeLoginParser.first_login_id);
								} else {
									// The Custom Toast Layout Imported here
									LayoutInflater inflater = getLayoutInflater();
									View layout = inflater.inflate(R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));
									 
									// The actual toast generated here.
									Toast toast = new Toast(getApplicationContext());
									toast.setDuration(Toast.LENGTH_LONG);
									toast.setView(layout);
									toast.show();
									startActivity(new Intent(FirstTimeLoginActivity.this, LoginScreenActivity.class));
									FirstTimeLoginActivity.this.finish();
									//showDialog(NO_NETWORK_CON);
								}
							} else {
								showDialog(SELECT_AGREE);
							}
						} else {
							showDialog(SELECT_COUNTRY);
						}
					} else {
						showDialog(DIALOG_SEND_INVALID_EMAIL);
					}
				} else {
					showDialog(REQUIRED_PASSWORD);
				}
			} else {
				// custom alert for mandatory fields.....Hari
				showDialog(FIELDS_ERROR);
			}
		} else if (v.getId() == R.id.closeftlBtnID) {
			startActivity(new Intent(FirstTimeLoginActivity.this, LoginScreenActivity.class));
			FirstTimeLoginActivity.this.finish();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent in = new Intent(getApplicationContext(),	LoginScreenActivity.class);
			startActivity(in);
			FirstTimeLoginActivity.this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	// Custom Dialogs implementation
	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case FIELDS_ERROR:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.alert_first_login_all_fields_error, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				fieldsDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				fieldsDialog.show();
				break;
			}
			return fieldsDialog;
		} else if (id == 2) {
			AlertDialog successDialog = null;
			switch (id) {
			case SUCCESS_REG:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.alert_success_or_failed_first_login, null);
				AlertDialog.Builder successalert = new AlertDialog.Builder(this);
				successalert.setView(allFieldsView);
				successDialog = successalert.create();
				successDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				successDialog.show();
				break;
			}
			return successDialog;
		}
		if (id == 3) {
			AlertDialog pswdfieldsDialog = null;
			switch (id) {
			case REQUIRED_PASSWORD:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dialog_first_login_password_required, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				pswdfieldsDialog = allFalert.create();
				pswdfieldsDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				pswdfieldsDialog.show();
				break;
			}
			return pswdfieldsDialog;
		} else if (id == 4) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case DIALOG_SEND_INVALID_EMAIL:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dialog_send_a_frnd_invalid, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				fieldsDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				fieldsDialog.show();
				break;
			}
			return fieldsDialog;
		} else if (id == 5) {
			AlertDialog successDialog = null;
			switch (id) {
			case FAILED_REG:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.alert_success_or_failed_first_login, null);
				AlertDialog.Builder successalert = new AlertDialog.Builder(this);
				successalert.setView(allFieldsView);
				successDialog = successalert.create();
				successDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				successDialog.show();
				break;
			}
			return successDialog;
		} else if (id == 6) {
			AlertDialog country_select = null;
			switch (id) {
			case SELECT_COUNTRY:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.alert_first_login_all_fields_error, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				country_select = allFalert.create();
				country_select.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				country_select.show();
				break;
			}
			return country_select;
		} else if (id == 7) {
			AlertDialog agree_select = null;
			switch (id) {
			case SELECT_AGREE:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.alert_first_login_all_fields_error, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				agree_select = allFalert.create();
				agree_select.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				agree_select.show();
				break;
			}
			return agree_select;
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
			switch (id) {
			case FIELDS_ERROR:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTilteTv = (TextView) alertDialog1
						.findViewById(R.id.allFieldsTVID);
				alertTilteTv.setTypeface(Utility.font_bold);
				allFieldsTV = (TextView) alertDialog1
						.findViewById(R.id.allFieldsMessageTVID);
				allFieldsTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog1
						.findViewById(R.id.allFieldsOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 2) {
			switch (id) {
			case SUCCESS_REG:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				alertTilteTv = (TextView) alertDialog2
						.findViewById(R.id.successTitleTVID);
				alertTilteTv.setTypeface(Utility.font_bold);
				successTV = (TextView) alertDialog2
						.findViewById(R.id.successTVID);
				successTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2
						.findViewById(R.id.successOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent in = new Intent(getApplicationContext(),
								LoginScreenActivity.class);
						startActivity(in);
						FirstTimeLoginActivity.this.finish();
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} else if (id == 3) {
			switch (id) {
			case REQUIRED_PASSWORD:
				final AlertDialog alertDialog3 = (AlertDialog) dialog;
				alertTilteTv = (TextView) alertDialog3
						.findViewById(R.id.pswdFieldsTVID);
				alertTilteTv.setTypeface(Utility.font_bold);
				passwdTV = (TextView) alertDialog3
						.findViewById(R.id.pswdFieldsMessageTVID);
				passwdTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog3
						.findViewById(R.id.pswdFieldsOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog3.dismiss();
					}
				});
				break;
			}
		} else if (id == 4) {
			final AlertDialog alertDialog1 = (AlertDialog) dialog;
			alertTilteTv = (TextView) alertDialog1
					.findViewById(R.id.emailInValidTitleTVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			textTV = (TextView) alertDialog1
					.findViewById(R.id.inValidEmailFieldTVID);
			textTV.setTypeface(Utility.font_reg);
			Button okbutton = (Button) alertDialog1
					.findViewById(R.id.inValidOEmailKBtnID);
			okbutton.setTypeface(Utility.font_bold);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog1.dismiss();
				}
			});
		} else if (id == 5) {
			switch (id) {
			case FAILED_REG:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				alertTilteTv = (TextView) alertDialog2
						.findViewById(R.id.successTitleTVID);
				alertTilteTv.setTypeface(Utility.font_bold);
				alertTilteTv.setText("Failed !");
				successTV = (TextView) alertDialog2
						.findViewById(R.id.successTVID);
				successTV.setTypeface(Utility.font_reg);
				successTV.setText("Please enter valid details.");
				okbutton = (Button) alertDialog2
						.findViewById(R.id.successOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} else if (id == 6) {
			switch (id) {
			case SELECT_COUNTRY:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTilteTv = (TextView) alertDialog1
						.findViewById(R.id.allFieldsTVID);
				alertTilteTv.setText("Required !");
				alertTilteTv.setTypeface(Utility.font_bold);
				allFieldsTV = (TextView) alertDialog1
						.findViewById(R.id.allFieldsMessageTVID);
				allFieldsTV.setText("Please select your country and state.");
				allFieldsTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog1
						.findViewById(R.id.allFieldsOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 7) {
			switch (id) {
			case SELECT_AGREE:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTilteTv = (TextView) alertDialog1
						.findViewById(R.id.allFieldsTVID);
				alertTilteTv.setText("Required !");
				alertTilteTv.setTypeface(Utility.font_bold);
				allFieldsTV = (TextView) alertDialog1
						.findViewById(R.id.allFieldsMessageTVID);
				allFieldsTV.setText("Please select 'I Agree'.");
				allFieldsTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog1
						.findViewById(R.id.allFieldsOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType == 15) {
				if (response == null) {
					showDialog(FAILED_REG);
				}
				if (response != null) {
					if (response instanceof String) {
						showDialog(SUCCESS_REG);
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
		}
	}

	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = true;
		if (v.getId() == R.id.firstNameETID) {
			if (firstNameEt.getText().length() > 25) {
				returnValue = true;
			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.lastNameETID) {
			if (lastNameEt.getText().length() > 25) {
				returnValue = true;
			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.passwdETID) {
			if (passwdEt.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.confPasswdETID) {
			if (confpasswdEt.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.emailETID) {
			if (emailEt.getText().length() > 60) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		}
		return returnValue;
	}

}
