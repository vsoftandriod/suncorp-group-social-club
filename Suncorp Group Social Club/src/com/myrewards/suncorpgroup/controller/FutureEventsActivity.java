package com.myrewards.suncorpgroup.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myrewards.suncorpgroup.service.CWUService;
import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.utils.Utility;

@SuppressLint("SetJavaScriptEnabled")
public class FutureEventsActivity extends Activity implements
		CWUServiceListener, OnClickListener {
	View loading;
	WebView webView;
	Button backButton;
	String temp;
	TextView titleTV;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_union);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.my_events));

		webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setLoadsImagesAutomatically(true);

		WebSettings settings = webView.getSettings();
		settings.setDefaultTextEncodingName("utf-8");
		
		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backButton.setOnClickListener(this);

		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		if (Utility
				.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			loading.setVisibility(View.VISIBLE);
			CWUService.getCWUService().sendTestRequestAbout(this);
		} else {
			if (Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				File root = new File(Environment.getExternalStorageDirectory()
						+ "/AWU");
				File file = new File(root, "myunion.html");
				if (file.exists()) {
					// Do action
					webView.loadUrl("file://"
							+ Environment.getExternalStorageDirectory()
							+ "/AWU" + "/myunion.html");
				}
			}
		}
		
	
	}

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		loading.setVisibility(View.GONE);
		if (response != null) {
			String responseSrting = response.toString();
			temp = responseSrting.split("<root>")[0];
			responseSrting = responseSrting.split("<root>")[1];
			responseSrting = responseSrting.replaceFirst("</root>", "</body>");
			String summary = "<html>" + "<body>" + responseSrting + "</html>";
			summary = Html.fromHtml(summary).toString();
					
			// create text file
			if (!Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED))
				Log.d("AWU", "No SDCARD");
			else {
				File direct = new File(
						Environment.getExternalStorageDirectory() + "/AWU");

				if (!direct.exists()) {
					if (direct.mkdir()) {
						// directory is created;
					}
				}

				try {
					File root = new File(
							Environment.getExternalStorageDirectory() + "/AWU");
					if (root.canWrite()) {
						File file = new File(root, "myunion.html");
						FileWriter fileWriter = new FileWriter(file);
						BufferedWriter out = new BufferedWriter(fileWriter);
						out.write(summary);
						out.close();
					}
				} catch (IOException e) {
					Log.e("AWU", "Could not write file " + e.getMessage());
				}
			}

			if (!Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				Log.d("AWU", "No SDCARD");
			} else {
				webView.loadUrl("file://"
						+ Environment.getExternalStorageDirectory() + "/AWU"
						+ "/myunion.html");
			}
		}
	}

	public static String getTagValueWithMultiItem(Element eElement) {
		String returnVal = "";
		Node eNode;
		int NumOFItem = eElement.getElementsByTagName("link").getLength();
		for (int y = 0; y < NumOFItem; y++) {
			eNode = eElement.getElementsByTagName("link").item(y);
			NamedNodeMap attributes = eNode.getAttributes();
			for (int g = 0; g < attributes.getLength(); g++) {
				Attr attribute = (Attr) attributes.item(g);
				if (attribute.getNodeName().equals("rel")
						&& attribute.getNodeValue().equals("alternate")) {
					try {
						returnVal = eNode.getAttributes().getNamedItem("href").getNodeValue();
					} catch (Exception e) {
						returnVal = e.toString();
					}
				}
			}
		}
		return returnVal;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backBtnID) {
			try {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				FutureEventsActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				FutureEventsActivity.this.finish();
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			try {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				FutureEventsActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				FutureEventsActivity.this.finish();
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}
