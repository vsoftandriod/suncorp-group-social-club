package com.myrewards.suncorpgroup.controller;


import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myrewards.suncorpgroup.utils.ApplicationConstants;
import com.myrewards.suncorpgroup.utils.Utility;

public class GINNoticeBoardDetailsActivity extends BaseActivity implements OnClickListener {
	View loading;
	String noticeId;
	String noticeSubject = null;
	String noticeDetails = null;
	Button backBtn;
	TextView subjectTV;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notice_board_details_gin);
		setHeaderTitle(getResources().getString(R.string.my_notice_board));
		RelativeLayout v=(RelativeLayout)findViewById(R.id.headerRLID);
		v.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		v.setVisibility(View.VISIBLE);
		showBackButton();
		//setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER_HORIZONTAL|Gravity.CENTER);
		menuBtn=(Button)findViewById(R.id.menuBtnID);
		backBtn=(Button)findViewById(R.id.backToSearchBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setOnClickListener(this);
		menuListView=(ListView)findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View)findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		
		if (getIntent() != null) {
			int colorResource = 0;
			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				noticeId = bundle.getString(ApplicationConstants.NOTICE_ID_KEY_GIN);
				noticeSubject = bundle.getString(ApplicationConstants.NOTICE_NAME_KEY_GIN);
				noticeDetails = bundle.getString(ApplicationConstants.NOTICE_DETAILS_KEY_GIN);
				colorResource = bundle.getInt(ApplicationConstants.COLOR_CODE_KEY);
			}
			View resultListItem = (View)findViewById(R.id.resultListItemID);
			switch(colorResource){
			case 0:
				resultListItem.setBackgroundResource(R.color.result_color_one);
				break;
			case 1:
				resultListItem.setBackgroundResource(R.color.result_color_two);
				break;
			case 2:
				resultListItem.setBackgroundResource(R.color.result_color_three);
				break;
			case 3:
				resultListItem.setBackgroundResource(R.color.result_color_four);
				break;
			}
			LinearLayout rowLL2 = (LinearLayout) findViewById(R.id.resultListItemHariLLID);
			rowLL2.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL | Gravity.CENTER);
			rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 9.4);
			TextView offNameTV = (TextView)findViewById(R.id.offerTVID);
			offNameTV.setVisibility(View.GONE);
			TextView noticeNameTV = (TextView)findViewById(R.id.productTVID);
			noticeNameTV.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL | Gravity.CENTER);
			noticeNameTV.setTypeface(Utility.font_bold);
			noticeNameTV.setText(""+noticeSubject);
			if (colorResource%2==0) {
				noticeNameTV.setTextColor(Color.parseColor("#9CEAE3"));
			}
			else if (colorResource%2==1) {
				noticeNameTV.setTextColor(Color.parseColor("#359A8D"));
			}
			TextView noticeDetailsTV = (TextView)findViewById(R.id.noticeBoardDetailsTVID);
			noticeDetailsTV.setTypeface(Utility.font_reg);
			noticeDetailsTV.setText(" "+Html.fromHtml(noticeDetails));
		}
	}
		@Override
		public void onClick(View arg0) {
			switch (arg0.getId()) {
			case R.id.backToSearchBtnID:
				finish();
				break;
			}
	}
}
