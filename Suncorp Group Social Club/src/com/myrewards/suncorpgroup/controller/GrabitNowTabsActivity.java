package com.myrewards.suncorpgroup.controller;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.suncorpgroup.utils.DatabaseHelper;
import com.myrewards.suncorpgroup.utils.Utility;

@SuppressWarnings("deprecation")
public class GrabitNowTabsActivity extends TabActivity implements
		OnClickListener {
	Button Btn1, Btn2, Btn3, Btn4, Btn5, Btn6, Btn7, homeBtn, logoutBtn,
			backBtn;
	TextView titleTV;
	TabHost tabHost;
	RelativeLayout menuBar;
	Button scanBarID;

	Context context;
	DatabaseHelper helper;

	public ImageView noticeboardunread;

	TabSpec tabSpec1, tabSpec2, tabSpec3, tabSpec4, tabSpec5, tabSpec6,
			tabSpec7;
	HorizontalScrollView horizontalScrollView;
	int[] titlesIDsArray = { R.string.hot_offers_text, R.string.search_text,
			R.string.daily_deals_text, R.string.whats_around_me_text,
			R.string.my_favorites_text,R.string.help,R.string.my_notice_board };
	
	int[] buttonIDsArray = { R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4,
			R.id.btn5, R.id.btn6, R.id.btnHotOffers };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.grabitnow_scroll_view);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		menuBar = (RelativeLayout) findViewById(R.id.menuRLID);
		menuBar.getLayoutParams().height = (int) (Utility.screenHeight / 8.5);

		Btn1 = (Button) findViewById(R.id.btn1);
		Btn2 = (Button) findViewById(R.id.btn2);
		Btn3 = (Button) findViewById(R.id.btn3);
		Btn4 = (Button) findViewById(R.id.btn4);
		Btn5 = (Button) findViewById(R.id.btn5);
		Btn6 = (Button) findViewById(R.id.btn6);
		backBtn = (Button) findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		scanBarID = (Button) findViewById(R.id.scanBtnID);
		scanBarID.setVisibility(View.VISIBLE);
		scanBarID.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		scanBarID.getLayoutParams().height = (int) (Utility.screenHeight / 13.5);
		setButtonsDimentions();

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		Btn1.setOnClickListener(this);
		Btn2.setOnClickListener(this);
		Btn3.setOnClickListener(this);
		Btn4.setOnClickListener(this);
		Btn5.setOnClickListener(this);
		Btn6.setOnClickListener(this);
		backBtn.setOnClickListener(this);
		scanBarID.setOnClickListener(this);

		tabHost = (TabHost) findViewById(android.R.id.tabhost);
		tabSpec1 = tabHost.newTabSpec("tabSpec1");
		tabSpec2 = tabHost.newTabSpec("tabSpec2");
		tabSpec3 = tabHost.newTabSpec("tabSpec3");
		tabSpec4 = tabHost.newTabSpec("tabSpec4");
		tabSpec5 = tabHost.newTabSpec("tabSpec5");
		tabSpec6 = tabHost.newTabSpec("tabSpec6");
		tabSpec7 = tabHost.newTabSpec("tabSpec7");

		tabSpec1.setIndicator("").setContent(new Intent(this, HotOffersActivity.class));
		tabSpec2.setIndicator("").setContent(new Intent(this, SearchListActivity.class));
		tabSpec3.setIndicator("").setContent(new Intent(this, DailyDealsActivity.class));
		tabSpec4.setIndicator("").setContent(new Intent(this, WhatsAroundMeActivity.class));
		tabSpec5.setIndicator("").setContent(new Intent(this, FavoritesListActivity.class));
		tabSpec6.setIndicator("").setContent(new Intent(this, HelpPagesActivity.class));
		tabSpec7.setIndicator("").setContent(new Intent(this, MyNoticeBoardActivity.class));
		tabHost.addTab(tabSpec1);
		tabHost.addTab(tabSpec2);
		tabHost.addTab(tabSpec3);
		tabHost.addTab(tabSpec4);
		tabHost.addTab(tabSpec5);
		tabHost.addTab(tabSpec6);
		tabHost.addTab(tabSpec7);

		// check on which dashboard icon user tapped
		if (getIntent() != null) {
			int number = 0;
			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				number = bundle.getInt(Utility.DASHBOARD_ICON_ID);
				if (number != 1) {
					getTabHost().setCurrentTabByTag("tabSpec" + number);
					titleTV.setText(getResources().getString(
							titlesIDsArray[number - 1]));
					enableMenuButtons(((Button) findViewById(buttonIDsArray[number - 1])));
				} else {
					getTabHost().setCurrentTabByTag("tabSpec" + number);
					titleTV.setText(getResources().getString(
							titlesIDsArray[number - 1]));
				}
			}
		}
	}

	private void setButtonsDimentions() {
		setButtonDimentions(Btn1);
		setButtonDimentions(Btn2);
		setButtonDimentions(Btn3);
		setButtonDimentions(Btn4);
		setButtonDimentions(Btn5);
		setButtonDimentions(Btn6);
	}

	private void setButtonDimentions(Button Btn) {
		Btn.getLayoutParams().width = Utility.screenWidth / 6;
		Btn.getLayoutParams().height = Utility.screenWidth / 6;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn1:
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				getTabHost().setCurrentTabByTag("tabSpec7");
				titleTV.setText(getResources().getString(titlesIDsArray[6]));
				enableMenuButtons(Btn1);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
							 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;
		case R.id.btn2:
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				getTabHost().setCurrentTabByTag("tabSpec2");
				titleTV.setText(getResources().getString(titlesIDsArray[1]));
				enableMenuButtons(Btn2);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
							 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;
		case R.id.btn3:
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				getTabHost().setCurrentTabByTag("tabSpec3");
				titleTV.setText(getResources().getString(titlesIDsArray[2]));
				enableMenuButtons(Btn3);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
							 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;
		case R.id.btn4:
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				getTabHost().setCurrentTabByTag("tabSpec4");
				titleTV.setText(getResources().getString(titlesIDsArray[3]));
				enableMenuButtons(Btn4);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
							 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;
		case R.id.btn5:
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				getTabHost().setCurrentTabByTag("tabSpec5");
				titleTV.setText(getResources().getString(titlesIDsArray[4]));
				enableMenuButtons(Btn5);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
							 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
			break;
		case R.id.btn6:
			getTabHost().setCurrentTabByTag("tabSpec6");
			titleTV.setText(getResources().getString(titlesIDsArray[5]));
			enableMenuButtons(Btn6);
			break;
		case R.id.backBtnID:
			startActivity(new Intent(GrabitNowTabsActivity.this, DashboardScreenActivity.class));
			GrabitNowTabsActivity.this.finish();
			break;
		case R.id.scanBtnID:
			try {
				startActivity(new Intent(GrabitNowTabsActivity.this, BarCodeScannerActivity.class));
			} catch (Exception e) {
				e.getStackTrace();
			}
		}
	}

	private void enableMenuButtons(Button disableBtn) {
		Btn1.setEnabled(true);
		Btn2.setEnabled(true);
		Btn3.setEnabled(true);
		Btn4.setEnabled(true);
		Btn5.setEnabled(true);
		Btn6.setEnabled(true);
		disableBtn.setEnabled(false);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			try {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				GrabitNowTabsActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				GrabitNowTabsActivity.this.finish();
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}
