package com.myrewards.suncorpgroup.controller;

import java.util.List;
import java.util.Vector;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;

import com.myrewards.suncorpgroup.model.HelpPageFive;
import com.myrewards.suncorpgroup.model.HelpPageFour;
import com.myrewards.suncorpgroup.model.HelpPageOne;
import com.myrewards.suncorpgroup.model.HelpPageSix;
import com.myrewards.suncorpgroup.model.HelpPageThree;
import com.myrewards.suncorpgroup.model.HelpPageTwo;


/**
 * The <code>ViewPagerFragmentActivity</code> class is the fragment activity hosting the ViewPager  
 * @author mwho
 */
public class HelpPagesActivity extends FragmentActivity{
	/** maintains the pager adapter*/
	private PagerAdapter mPagerAdapter;
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.helpviewpager_layout);
		//initialsie the pager
		this.initialisePaging();
	}
	
	/**
	 * Initialise the fragments to be paged
	 */
	private void initialisePaging() {
		
		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, HelpPageOne.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageTwo.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageThree.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFour.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFive.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageSix.class.getName()));
		this.mPagerAdapter  = new PagerAdapter(super.getSupportFragmentManager(), fragments);
		//
		ViewPager pager = (ViewPager)super.findViewById(R.id.viewpager);
		pager.setAdapter(this.mPagerAdapter);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			try {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				HelpPagesActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				HelpPagesActivity.this.finish();
			}
		}
		return super.onKeyDown(keyCode, event);
	}

}
