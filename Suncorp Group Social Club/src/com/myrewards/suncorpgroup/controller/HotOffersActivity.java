package com.myrewards.suncorpgroup.controller;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.suncorpgroup.model.Product;
import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.service.GrabItNowService;
import com.myrewards.suncorpgroup.utils.ApplicationConstants;
import com.myrewards.suncorpgroup.utils.Utility;

@SuppressLint("ShowToast")
public class HotOffersActivity extends BaseActivity implements
		CWUServiceListener {
	List<Product> hotOffersProductsList;
	HotOffersAdapter mAdapter;
	LayoutInflater inflater;
	View loading;
	ListView hotOffersListView;
	String catID = null;
	String location = null;
	String keyword = null;
	// this is header Relative Layout
	RelativeLayout headerRL;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);
		setHeaderTitle(getResources().getString(R.string.hot_offers_text));
		headerRL = (RelativeLayout) findViewById(R.id.headerRLID);
		headerRL.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		headerRL.setVisibility(View.GONE);
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		View header = (View) findViewById(R.id.headerID);
		header.setVisibility(View.GONE);
		hotOffersListView = (ListView) findViewById(R.id.resultsListViewID);
		hotOffersProductsList = new ArrayList<Product>();
		hotOffersListView.setOnItemClickListener(this);
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			GrabItNowService.getGrabItNowService().sendHotOffersRequest(this);
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			HotOffersActivity.this.finish();
		}
	}

	@SuppressLint("ResourceAsColor")
	public class HotOffersAdapter extends BaseAdapter {
		Context ctx;

		public HotOffersAdapter(HotOffersActivity hotOffersActivity) {
			this.ctx = hotOffersActivity;
		}

		@Override
		public int getCount() {
			return hotOffersProductsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View resultsListRow = null;
			if (resultsListRow == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				resultsListRow = (View) inflater.inflate(
						R.layout.results_list_item, null, false);
			}
			LinearLayout rowLL = (LinearLayout) resultsListRow
					.findViewById(R.id.resultListItemLLID);
			LinearLayout rowLL2 = (LinearLayout) resultsListRow
					.findViewById(R.id.resultListItemHariLLID);
			rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 8.5);
			TextView productNameTV = (TextView) resultsListRow
					.findViewById(R.id.productTVID);
			productNameTV.setTypeface(Utility.font_bold);
			TextView highlightTV = (TextView) resultsListRow
					.findViewById(R.id.offerTVID);
			productNameTV.setText(hotOffersProductsList.get(pos).getName());
			highlightTV.setTypeface(Utility.font_reg);
			highlightTV.setText(hotOffersProductsList.get(pos).getHighlight());
			switch (pos % 2) {
			case 0:
				rowLL.setBackgroundResource(R.color.result_color_one);
				productNameTV.setTextColor(Color.parseColor("#9CEAE3"));
				break;
			case 1:
				rowLL.setBackgroundResource(R.color.result_color_two);
				productNameTV.setTextColor(Color.parseColor("#359A8D"));
				break;
			}
			Animation animation = AnimationUtils.loadAnimation(ctx,
					R.anim.push_left_in);
			resultsListRow.startAnimation(animation);
			animation = null;
			return resultsListRow;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (menuListView.getVisibility() == ListView.GONE) {
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				Intent detailsIntent = new Intent(HotOffersActivity.this, ProductDetailsActivity.class);
				detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,
						hotOffersProductsList.get(pos).getId());
				detailsIntent
						.putExtra(ApplicationConstants.COLOR_CODE_KEY, pos % 2);
				detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY,
						hotOffersProductsList.get(pos).getName());
				detailsIntent.putExtra(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY,
						hotOffersProductsList.get(pos).getHighlight());
				startActivity(detailsIntent);
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (response != null) {
				if (response instanceof String) {
					// showErrorDialog(response.toString());
					Utility.showMessage(this, response.toString());
				} else {
					if (eventType != 16) {
						hotOffersProductsList = (ArrayList<Product>) response;
						mAdapter = new HotOffersAdapter(this);
						hotOffersListView.setAdapter(mAdapter);
						loading.setVisibility(View.GONE);
					}
				}
			}
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(),	"Server busy. Please wait or try again", Toast.LENGTH_LONG).show();
			if (e != null) {
				e.printStackTrace();
			}
		}
	}
}
