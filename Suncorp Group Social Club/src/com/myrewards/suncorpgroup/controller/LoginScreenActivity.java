package com.myrewards.suncorpgroup.controller;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myrewards.suncorpgroup.cache.WebImageCache;
import com.myrewards.suncorpgroup.model.LoginDetails;
import com.myrewards.suncorpgroup.model.NoticeId;
import com.myrewards.suncorpgroup.model.User;
import com.myrewards.suncorpgroup.service.CWUService;
import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.service.GrabItNowService;
import com.myrewards.suncorpgroup.timer.CrittercismAndroidByHari;
import com.myrewards.suncorpgroup.utils.ApplicationConstants;
import com.myrewards.suncorpgroup.utils.DatabaseHelper;
import com.myrewards.suncorpgroup.utils.Utility;
import com.myrewards.suncorpgroup.xml.FirsttimeLoginParser;

@SuppressWarnings("deprecation")
@SuppressLint("SimpleDateFormat")
public class LoginScreenActivity extends Activity implements OnClickListener,
		OnLongClickListener, CWUServiceListener {
	public static EditText etUserId;
	public static EditText etPasswd;
	RelativeLayout parentRL;
	@SuppressWarnings("unused")
	private Dialog messageDialog;
	LinearLayout parentSubLayout;
	ArrayAdapter<String> adapter;
	Button btnLogin;
	private InputMethodManager imm;
	DatabaseHelper dbHelper;
	ProgressDialog myPd_ring;
	boolean isWithinOneWeek = false;
	int number = 0;
	SimpleDateFormat formatter;
	ArrayList<NoticeId> noticeid;
	LoginDetails loginDetails;
	User userdetails;
	RelativeLayout loadingPanel;
	Dialog dialog;

	// First time Login details
	public static Button firstTimeLoginBtn;
	public static EditText msNoet;
	public static AlertDialog dialogDetails2;

	public Animation movement5;
	LinearLayout loginLL;
	TextView invaliFieldMembership;

	final private static int FIRST_TIME_LOGIN_BUTTON = 1;
	final private static int MEMBERSHIP_NO = 2;
	final private static int NO_NETWORK_CON = 3;
	final private static int LOGIN_CORRECT = 4;
	final private static int WRONG_PASSWORD = 5;
	final private static int WRONG_USERNAME = 6;

	public TextView tv12;
	public Button okbutton;
	
	public void deleteCmpleteCacheData()
	{
		try{
			deleteCache(this);
			WebImageCache cache=new WebImageCache(this);
			cache.clear();
			}
			catch(Exception e)
			{
				if(e!=null)
				{
					e.printStackTrace();
				}
			}
	}
	public static void deleteCache(Context context) {
	    try {
	        File dir = context.getCacheDir();
	        if (dir != null && dir.isDirectory()) {
	            deleteDir(dir);
	        }
	    } catch (Exception e) {}
	}
	public static boolean deleteDir(File dir) {
	    if (dir != null && dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i = 0; i < children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }
	    return dir.delete();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		deleteCmpleteCacheData();
		
		setDimensions();
	
		
		
		
		loadingPanel = (RelativeLayout) findViewById(R.id.loadingPanelNew);

		dbHelper = new DatabaseHelper(LoginScreenActivity.this);
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			GrabItNowService.getGrabItNowService().sendNoticeBoardCountIdRequset(this);
		} else {
			showDialog(NO_NETWORK_CON);
		}

		etUserId = (EditText) findViewById(R.id.usernameETID);
		etUserId.setTypeface(Utility.font_reg);
		etUserId.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		etPasswd = (EditText) findViewById(R.id.passwordETID);
		etPasswd.getLayoutParams().height = (int) (Utility.screenHeight / 16);

		etUserId.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		etPasswd.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.loginParentRLID);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(Utility.screenWidth / 32, Utility.screenWidth / 32,
				Utility.screenWidth / 32, Utility.screenWidth / 32);
		params.weight = 1.1f;
		rl.setLayoutParams(params);

		etUserId.setOnLongClickListener(this);
		etPasswd.setOnLongClickListener(this);
		// etUserId.addTextChangedListener(this);

		btnLogin = (Button) findViewById(R.id.loginBtnID);
		btnLogin.getLayoutParams().height = (int) (Utility.screenHeight / 16);

		btnLogin.setOnClickListener(this);
		if (getIntent() != null) {
			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				number = bundle.getInt(Utility.DASHBOARD_ICON_ID);
			}
		}

		loginLL = (LinearLayout) findViewById(R.id.loginLLID);
		movement5 = AnimationUtils.loadAnimation(this, R.anim.accelerate_one);
		loginLL.startAnimation(movement5);
		try {
			loginDetails = dbHelper.getLoginDetails();
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
		}
		Date loginDate = null;
		Date currentDate = null;

		if (loginDetails != null) {
			loginLL.setVisibility(View.GONE);
			formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			/*
			 * DateFormat formatter = DateFormat.getDateTimeInstance(
			 * DateFormat.MEDIUM, DateFormat.SHORT);
			 */
			try {
				loginDate = formatter.parse(loginDetails.getLoginDate());
				// loginDate =formatter.format(new Date(o));
			} catch (Exception e) {
				loginLL.setVisibility(View.VISIBLE);
				loadingPanel.setVisibility(View.GONE);
				if (e != null) {
					e.printStackTrace();
				}
			}
			// String currentDateString =
			// DateFormat.getDateTimeInstance().format(new Date());
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			String currentDateString = dateFormat.format(cal.getTime());
			try {
				currentDate = formatter.parse(currentDateString);
			} catch (ParseException e) {
				loginLL.setVisibility(View.VISIBLE);
				loadingPanel.setVisibility(View.GONE);
				// TODO Auto-generated catch block
				if (e != null) {
					e.printStackTrace();
				}
			}

			long diff = currentDate.getTime() - loginDate.getTime();
			long days = diff / (1000 * 60 * 60 * 24);
		
			if (days<7) {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					CWUService.getCWUService().sendLoginRequest(this,loginDetails.getUsername(), loginDetails.getPassword(),loginDetails.getSubDomain());
				} else {
					showDialog(NO_NETWORK_CON);
				}
			} else {
				
				try {
					dbHelper.deleteLoginDetails();
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
					}
				}
				loginDetails=null;
				
				loginLL.setVisibility(View.VISIBLE);
				loadingPanel.setVisibility(View.GONE);
				movement5 = AnimationUtils.loadAnimation(this, R.anim.accelerate_one);
				loginLL.startAnimation(movement5);
			}
		} else {
			loginLL.setVisibility(View.VISIBLE);
			loadingPanel.setVisibility(View.GONE);
			movement5 = AnimationUtils.loadAnimation(this, R.anim.accelerate_one);
			loginLL.startAnimation(movement5);
		}
	
		try {
			userdetails = dbHelper.getUserDetails();
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
		}
		
		firstTimeLoginBtn = (Button) findViewById(R.id.firstTimeloginBtnID);
		firstTimeLoginBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		firstTimeLoginBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialog(FIRST_TIME_LOGIN_BUTTON);
			}
		});
	}

	

	

	

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog dialogDetails = null;
			switch (id) {
			case FIRST_TIME_LOGIN_BUTTON:
				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater
						.inflate(R.layout.dialog_layout, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
						this);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails.show();

				break;
			}
			return dialogDetails;
		} else if (id == 2) {
			AlertDialog dialogDetails2 = null;
			switch (id) {
			case MEMBERSHIP_NO:
				LayoutInflater inflater2 = LayoutInflater.from(this);
				View dialogview = inflater2.inflate(
						R.layout.dialog_layout_membership_ok, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
						this);
				dialogbuilder.setTitle("Required!!");
				dialogbuilder.setIcon(R.drawable.alert1);
				dialogbuilder.setView(dialogview);
				dialogDetails2 = dialogbuilder.create();
				dialogDetails2.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails2.show();
				break;
			}
			return dialogDetails2;

		}

		else if (id == 3) {
			AlertDialog noNetworkDialog = null;
			switch (id) {
			case NO_NETWORK_CON:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_layout_no_network, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setView(noNetworkView);
				noNetworkDialog = adbNoNet.create();
				noNetworkDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noNetworkDialog.show();
				break;
			}
			return noNetworkDialog;
		} else if (id == 4) {
			AlertDialog loginFailedDialog = null;
			switch (id) {
			case LOGIN_CORRECT:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dailog_layout_login_faild, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setView(noNetworkView);
				loginFailedDialog = adbNoNet.create();
				loginFailedDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				loginFailedDialog.show();
				break;
			}
			return loginFailedDialog;
		} else if (id == 5) {
			AlertDialog loginFailedDialog = null;
			switch (id) {
			case WRONG_PASSWORD:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_password_failed, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setView(noNetworkView);
				loginFailedDialog = adbNoNet.create();
				loginFailedDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				loginFailedDialog.show();
				break;
			}
			return loginFailedDialog;
		} else if (id == 6) {
			AlertDialog loginFailedDialog = null;
			switch (id) {
			case WRONG_USERNAME:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_invalid_username, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setView(noNetworkView);
				loginFailedDialog = adbNoNet.create();
				loginFailedDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				loginFailedDialog.show();
				break;
			}
			return loginFailedDialog;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog1) {
		dialog = dialog1;
		if (id == 1) {
			switch (id) {
			case FIRST_TIME_LOGIN_BUTTON:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog1
						.findViewById(R.id.firstLoginTitleTVID);

				TextView alertMessage = (TextView) alertDialog1
						.findViewById(R.id.alertMsgTVID);

				alertTitle.setTypeface(Utility.font_bold);
				alertMessage.setTypeface(Utility.font_reg);

				Button submitButton = (Button) alertDialog1
						.findViewById(R.id.submitBtn);
				submitButton.setTypeface(Utility.font_bold);
				Button cancelbutton = (Button) alertDialog1
						.findViewById(R.id.btn_cancel);
				cancelbutton.setTypeface(Utility.font_bold);
				msNoet = (EditText) alertDialog1
						.findViewById(R.id.membershipCardETID);
				msNoet.setTypeface(Utility.font_reg);

				msNoet.setOnLongClickListener(this);
				submitButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (msNoet.getText().toString().length() > 0) {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								GrabItNowService.getGrabItNowService()
								.sendFirstTimeLoginDetails(
										LoginScreenActivity.this,
										msNoet.getText().toString(),
										ApplicationConstants.CLINT_DOMAIN_ADDRESS);
								alertDialog1.dismiss();
							} else {
								showDialog(NO_NETWORK_CON);
							}
						} else {
							Animation shake = AnimationUtils.loadAnimation(
									LoginScreenActivity.this, R.anim.shake);
							msNoet.startAnimation(shake);
						}
					}
				});
				cancelbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 2) {
			switch (id) {
			case MEMBERSHIP_NO:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.alertLogoutTitleTVID);

				alertTitle.setTypeface(Utility.font_bold);

				tv12 = (TextView) alertDialog2
						.findViewById(R.id.cardNoInvalidTVID);

				tv12.setTypeface(Utility.font_reg);

				okbutton = (Button) alertDialog2
						.findViewById(R.id.inValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} else if (id == 3) {
			switch (id) {
			case NO_NETWORK_CON:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.alertLogoutTitleTVID);

				alertTitle.setTypeface(Utility.font_bold);

				tv12 = (TextView) alertDialog2.findViewById(R.id.noConnTVID);
				tv12.setTypeface(Utility.font_reg);

				okbutton = (Button) alertDialog2
						.findViewById(R.id.noNetWorkOKID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						loadingPanel.setVisibility(View.GONE);
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} else if (id == 4) {
			switch (id) {
			case LOGIN_CORRECT:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				tv12 = (TextView) alertDialog2
						.findViewById(R.id.loginFieldTVID);

				tv12.setTypeface(Utility.font_reg);

				okbutton = (Button) alertDialog2
						.findViewById(R.id.loginFieldOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							dbHelper.deleteLoginDetails();
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
							}
						}
						loginLL.setVisibility(View.VISIBLE);
						loadingPanel.setVisibility(View.GONE);
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} else if (id == 5) {
			switch (id) {
			case WRONG_PASSWORD:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				tv12 = (TextView) alertDialog2
						.findViewById(R.id.inValidFieldTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2
						.findViewById(R.id.inValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} else if (id == 6) {
			switch (id) {
			case WRONG_USERNAME:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				tv12 = (TextView) alertDialog2
						.findViewById(R.id.inValidFieldTVID);
				tv12.setTypeface(Utility.font_reg);

				okbutton = (Button) alertDialog2
						.findViewById(R.id.inValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
				break;
			}
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.loginBtnID:
			String userId = etUserId.getText().toString();
			String passwd = etPasswd.getText().toString();
			String subDomainURL = ApplicationConstants.CLINT_DOMAIN_ADDRESS;

			if (userId != null && userId.trim().length() > 0) {
				if (passwd != null && passwd.trim().length() > 0) {
					loadingPanel.setVisibility(View.VISIBLE);
					issueRequest(userId, passwd, subDomainURL);

				} else {
					showDialog(WRONG_PASSWORD);
					btnLogin.setEnabled(true);
				}
			} else {
				showDialog(WRONG_USERNAME);
				btnLogin.setEnabled(true);
			}
			break;
		}
	}

	private void issueRequest(String userId, String password,
			String subDomainURL) {
		imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(etUserId.getWindowToken(), 0);
		imm.hideSoftInputFromWindow(etPasswd.getWindowToken(), 0);
		System.out.println("password: " + password);
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			loadingPanel.setVisibility(View.VISIBLE);
			CWUService.getCWUService().sendLoginRequest(this, userId, password, subDomainURL);
		} else {
			showDialog(NO_NETWORK_CON);
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			// loadingPanel.setVisibility(View.GONE);
			if (eventType != 14 && eventType != 16) {
				if (response != null) {
					if (response instanceof String) {
						showDialog(LOGIN_CORRECT);
						// Utility.showMessage(this, response.toString());
					} else {
						if (loginDetails == null) {
							DateFormat dateFormat = new SimpleDateFormat(
									"yyyy/MM/dd HH:mm:ss");
							Calendar cal = Calendar.getInstance();
							dbHelper.addLoginDetails(etUserId.getText().toString(),
									etPasswd.getText().toString(),
									ApplicationConstants.CLINT_DOMAIN_ADDRESS,
									dateFormat.format(cal.getTime()));
							Utility.user = (User) response;
							Utility.username = etUserId.getText().toString();
							Utility.password = etPasswd.getText().toString();
						}
						if (Utility.username == null) {
							Utility.username = loginDetails.getUsername();
							Utility.password = loginDetails.getPassword();
						}
						Utility.user = (User) response;
						
						try {
							if (dbHelper.getLoginDetails() != null) {
								if (dbHelper.getLoginDetails().getUsername() != null) {
									String uName = dbHelper.getLoginDetails().getUsername().toString();
									CrittercismAndroidByHari.SetUsername(uName);
								}
							}
						} catch (Exception e) {
							Log.w("Hari--> Login Page", e);
						}
						
						// this is for notifying the user regarding the updates of
						// notice boards
						try {
							if (dbHelper.getNoticeUpdateState().equals("NO_VALUE")) {
								dbHelper.setNoticeUpdateState("ON");
								Utility.setNotificationReceiver(this);
							}
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
							}
						}
						Intent i = new Intent(LoginScreenActivity.this,	DashboardScreenActivity.class);
						startActivity(i);
						LoginScreenActivity.this.finish();
					}
				}
			}

			else if (eventType == 16) {
				if (response != null) {
					noticeid = (ArrayList<NoticeId>) response;
					int i = -1;
					if (noticeid != null)
					{
						List<String> shambhiList = new ArrayList<String>();
						for (int n = 0; n < noticeid.size(); n++) {
							if (!(noticeid.get(n).getNoticeDetails() == null))
								shambhiList.add(noticeid.get(n).getNoticeDetails());
						}
						if (shambhiList.size() != dbHelper.getExistingIDs().size()) {
							for (int m = 0; m < dbHelper.getExistingIDs().size(); m++) {
								if (shambhiList.contains(dbHelper
										.getExistingNoticeRealIDs().get(m))) {

								} else {
									dbHelper.deleteNoticeIdDetails(dbHelper
											.getExistingNoticeRealIDs().get(m));

								}
							}
						}

						for (int k = 0; k < noticeid.size(); k++) {
							if (noticeid.get(k).getNoticeDetails() != null) {
								i++;
								List<String> list = dbHelper.getExistingIDs();
								if (!list.contains(noticeid.get(k)
										.getNoticeDetails())) {
									dbHelper.addnoticeiddetails(noticeid.get(k)
											.getNoticeDetails());
								}
							}
						}
					}
				}
			}

			else {
				if (response != null) {
					if (eventType == 14) {
						if ((FirsttimeLoginParser.firsttimeloginresponce)
								.equalsIgnoreCase("TRUE")) {
							Animation shake = AnimationUtils.loadAnimation(
									LoginScreenActivity.this, R.anim.shake);
							msNoet.startAnimation(shake);
						} else if ((FirsttimeLoginParser.firsttimeloginresponce)
								.equalsIgnoreCase("FALSE")) {
							startActivity(new Intent(LoginScreenActivity.this, FirstTimeLoginActivity.class));
							LoginScreenActivity.this.finish();
							dialog.dismiss();
						}
					}
				}
			}
		
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
		}
	}

	private void setDimensions() {
		parentSubLayout = (LinearLayout) findViewById(R.id.loginLLID);
		parentSubLayout.getLayoutParams().width = Utility.screenWidth-Utility.screenWidth / 5;
	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = true;
		if (v.getId() == R.id.usernameETID) {
			if (etUserId.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.passwordETID) {
			if (etPasswd.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.membershipCardETID) {
			if (msNoet.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		}
		return returnValue;
	}

}