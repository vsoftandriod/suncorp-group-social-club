package com.myrewards.suncorpgroup.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.suncorpgroup.model.User;
import com.myrewards.suncorpgroup.service.CWUService;
import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.utils.ApplicationConstants;
import com.myrewards.suncorpgroup.utils.DatabaseHelper;
import com.myrewards.suncorpgroup.utils.Utility;

@SuppressLint("ShowToast")
@SuppressWarnings({"unused","deprecation"})
public class MyAccountActivity extends Activity implements CWUServiceListener,
		OnClickListener, OnItemSelectedListener, OnLongClickListener {
	EditText firstnameET, lastnameET, emailET;// membershipnumberET

	EditText stateET, countryET;

	// custom dialog fields
	public TextView logoutText, alertTitleTV;
	public Button loginbutton1;
	public Button cancelbutton1;
		
	private Spinner stateSP2, countrySP1;
	View loading;
	Button backButton, scanBarBtn, submitBtn;
	TextView titleTV;
	private final static int UPDATE_ACCOUNT_SUCCUSS = 1;
	private final static int UPDATE_ACCOUNT_All_FIELDS = 2;
	private final static int NO_NETWORK_CON = 3;
	private final static int MY_ACCOUNT_STRINGS = 4;
	private static final int DIALOG_SEND_FAILED = 5;
	final private static int DIALOG_SEND_INVALID_EMAIL = 6;
	private final static int LOGOUT = 7;
	TextView tv11, tv12, textTV, textTV2;
	Button okbutton;
	
	int[] spinnerRIDS = { R.array.category_state_australia,
			R.array.category_state_hongkong, R.array.category_state_india,
			R.array.category_state_newzealand,
			R.array.category_state_philippines,
			R.array.category_state_singapore };
	String countries[] = { "Australia", "Hong Kong", "India", "New Zealand",
			"Philippines", "Singapore" };

	String stateSPITEMS[][] = {
			{ "", "ACT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA" },
			{ "HK" },
			{ "", "AP", "DL", "GJ", "HR", "KA", "MH", "TN", "UP", "WB" },
			{ "", "AUK", "BOP", "CAN", "FIL", "GIS", "HKB", "MBH", "MWT",
					"NAB", "NTL", "OTA", "STL", "TKI", "TMR", "TSM", "WGI",
					"WGN", "WKO", "WPP", "WTC" },
			{ "", "LUZ", "MIN", "NCR", "VIS" },
			{ "", "CS", "NES", "NWS", "SES", "SWS" } };

	String countrySPITEMS[] = { "", "AU", "HK", "IN", "NZ", "PH", "SG" };

	String cCode = "", stateName = null;
	ArrayAdapter<CharSequence> adapter2 = null;
	DatabaseHelper helper;

	String stateCode,mCountrySP,state ;
	
	// ProgressDialog myPd_ring;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_account);
		
		helper = new DatabaseHelper(MyAccountActivity.this);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5f);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.my_account));

		//scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		//scanBarBtn.setVisibility(View.GONE);
		
		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.VISIBLE);
		scanBarBtn.setBackgroundResource(R.drawable.logout_s);
		scanBarBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5f);
		scanBarBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0f);
		scanBarBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialog(LOGOUT);
			}
		});

		loading = (View) findViewById(R.id.loading);
		submitBtn = (Button) findViewById(R.id.submitBtnID);
		submitBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0f);
		submitBtn.setOnClickListener(this);
		submitBtn.setWidth(Utility.screenWidth / 2);
		ImageView logoIV = (ImageView) findViewById(R.id.logoIVID);
		logoIV.getLayoutParams().width = (int) (Utility.screenWidth / 3.5f);
		logoIV.getLayoutParams().height = (int) (Utility.screenWidth / 3.5f);

		firstnameET = (EditText) findViewById(R.id.firstnameETID);
		firstnameET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		firstnameET.setTypeface(Utility.font_reg);
		lastnameET = (EditText) findViewById(R.id.lastnameETID);
		lastnameET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		lastnameET.setTypeface(Utility.font_reg);
		firstnameET.setOnLongClickListener(this);
		lastnameET.setOnLongClickListener(this);

		TextView tv1 = (TextView) findViewById(R.id.firstNameAccountTVID);
		tv1.setTypeface(Utility.font_reg);
		TextView tv2 = (TextView) findViewById(R.id.lastNameAccountTVID);
		tv2.setTypeface(Utility.font_reg);
		TextView tv3 = (TextView) findViewById(R.id.emailAccountTVID);
		tv3.setTypeface(Utility.font_reg);
		TextView tv4 = (TextView) findViewById(R.id.stateAccountTVID);
		tv4.setTypeface(Utility.font_reg);
		TextView tv5 = (TextView) findViewById(R.id.stateAccountdTVID);
		tv5.setTypeface(Utility.font_reg);

		// stateET=(EditText)findViewById(R.id.stateETID);
		// membershipnumberET=(EditText)findViewById(R.id.membershipnumberETID);
		emailET = (EditText) findViewById(R.id.emailETID);
		emailET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		emailET.setTypeface(Utility.font_reg);
		emailET.setOnLongClickListener(this);

		stateET = (EditText) findViewById(R.id.stateETID);
		stateET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		stateET.setTypeface(Utility.font_reg);
		// stateET.setEnabled(false);
		stateET.setOnLongClickListener(this);
		stateET.setOnClickListener(this);
		countryET = (EditText) findViewById(R.id.countryETID);
		countryET.getLayoutParams().height = (int) (Utility.screenHeight / 16);
		countryET.setTypeface(Utility.font_reg);
		countryET.setOnLongClickListener(this);
		countryET.setOnClickListener(this);

		// back to dash board screen
		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5f);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0f);
		backButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(MyAccountActivity.this, DashboardScreenActivity.class));
				MyAccountActivity.this.finish();
			}
		});
		// select country
		countrySP1 = (Spinner) findViewById(R.id.countrySpinnerID);
		countrySP1.getLayoutParams().height = (int) (Utility.screenHeight / 16.6);
		countrySP1.setOnItemSelectedListener(this);

		// select state
		stateSP2 = (Spinner) findViewById(R.id.stateSpinnerID);
		stateSP2.getLayoutParams().height = (int) (Utility.screenHeight / 16.6);
		stateSP2.setOnItemSelectedListener(this);
		
		submitBtn.setEnabled(false);
		scanBarBtn.setEnabled(false);
		firstnameET.setEnabled(false);
		emailET.setEnabled(false);
		stateET.setEnabled(false);
		countryET.setEnabled(false);
		backButton.setEnabled(false);
		countrySP1.setEnabled(false);
		stateSP2.setEnabled(false);
		
		stateSP2.setVisibility(View.GONE);
		ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(
				this, R.array.category_countries,
				android.R.layout.simple_spinner_item);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		countrySP1.setAdapter(adapter1);

		countrySP1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				ArrayAdapter<CharSequence> adapter2 = null;
				if (arg2 == 0) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this, R.array.selectstatearray,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);

					}
				} else if (arg2 == 1) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_australia,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);

					}

				} else if (arg2 == 2) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_hongkong,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 3) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_india,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);

					}
				} else if (arg2 == 4) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_newzealand,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 5) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_philippines,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				} else if (arg2 == 6) {
					adapter2 = ArrayAdapter.createFromResource(
							MyAccountActivity.this,
							R.array.category_state_singapore,
							android.R.layout.simple_spinner_item);
					if (adapter2 != null) {
						adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						stateSP2.setAdapter(adapter2);
						stateSP2.setVisibility(View.VISIBLE);
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			CWUService.getCWUService().sendLoginRequest(this, Utility.username,	Utility.password, ApplicationConstants.CLINT_DOMAIN_ADDRESS);
		} else {
			showDialog(NO_NETWORK_CON);
			startActivity(new Intent(MyAccountActivity.this, DashboardScreenActivity.class));
			MyAccountActivity.this.finish();
		}
		loadEditBoxes();
	}

	private void loadEditBoxes() {
		try {
			Resources res = getResources();
			String[] str = null;
			if (Utility.user.getFirst_name() != null)
				firstnameET.setText(Utility.user.getFirst_name());
			
			if (Utility.user.getLast_name() != null)
				lastnameET.setText(Utility.user.getLast_name());
			
			if (Utility.user.getEmail() != null)
				emailET.setText(Utility.user.getEmail());
			
			for (int i = 0; i < countries.length; i++) {
				if (countries[i].equals(Utility.user.getCountry())) {
					str = res.getStringArray(spinnerRIDS[i]);

					for (int j = 0; j < str.length; j++) {
						if (stateSPITEMS[i][j].equals(Utility.user.getState())) {
							stateName = str[j];
							break;
						}
					}
				}

			}
			
			if (Utility.user.getState() != null) {
				stateET.setText(stateName);
			} else {
				stateET.setText("Select State");
			}
			if (Utility.user.getCountry() != null) {
				countryET.setText(Utility.user.getCountry());
			} else {
				countryET.setText("Select Country");
			}
			
			/*if (stateName != null)
				stateET.setText(stateName);
			else {
				stateET.setText("Select State");
			}
			if (Utility.user.getCountry() != null)
				countryET.setText(Utility.user.getCountry());
			else {
				countryET.setText("Select Country");
			}*/
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->Debug", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			loading.setVisibility(View.GONE);
			submitBtn.setEnabled(true);
			scanBarBtn.setEnabled(true);
			firstnameET.setEnabled(true);
			emailET.setEnabled(true);
			stateET.setEnabled(true);
			countryET.setEnabled(true);
			backButton.setEnabled(true);
			countrySP1.setEnabled(true);
			stateSP2.setEnabled(true);
			if (response != null && eventType == 9) {
				if (response instanceof String) {
					if (response.toString().contains("success")) {

						Utility.user.setFirst_name(firstnameET.getText()
								.toString());
						Utility.user.setLast_name(lastnameET.getText()
								.toString());
						Utility.user.setEmail(emailET.getText().toString());
						Utility.user.setCountry(mCountrySP);
						Utility.user.setState(stateCode);
						
						showDialog(UPDATE_ACCOUNT_SUCCUSS);
					} else
						showDialog(DIALOG_SEND_FAILED);
				}
			}

			else if (eventType != 14 && eventType != 16) {
				if (response != null) {
					if (response instanceof String) {
					} else {
						Utility.user = (User) response;
						loadEditBoxes();
					}
				}
			}
		
		} catch (Exception e) {
			if (e != null) {
			
				Toast.makeText(getApplicationContext(), "Server busy. Please wait or try again.", Toast.LENGTH_LONG).show();
				if (e != null) {
					e.printStackTrace();
				}
			}
		}
	}

	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.submitBtnID) {
			final String mEmail = emailET.getText().toString();
			String fName = firstnameET.getText().toString();
			String lName = lastnameET.getText().toString();

			if (stateSP2.getSelectedItem() != null
					&& countrySP1.getSelectedItem() != null) {
				
				 state = stateSP2.getSelectedItem().toString();
				 mCountrySP = countrySP1.getSelectedItem().toString();

				int countrySPPOS = countrySP1.getSelectedItemPosition();
				int stateSPPOS = stateSP2.getSelectedItemPosition();

				if (fName.length() < 1 || lName.length() < 1
						|| state.equals("Select State")
						|| mCountrySP.equals("Select Country")
						|| mEmail.length() < 1) {
					showDialog(UPDATE_ACCOUNT_All_FIELDS);
				} else {
					if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						if (isEmailValid(mEmail)) {
							
							stateCode=stateSPITEMS[countrySPPOS - 1][stateSPPOS];
							
							loading.setVisibility(View.VISIBLE);
								CWUService.getCWUService().sendMyAccountRequest(
										this,
										Utility.user.getId(),
										firstnameET.getText()
												.toString(),
										lastnameET.getText().toString(),
										mEmail,
										mCountrySP,
										stateCode,
										Utility.user.getNewsletter());	
							} 
					else {
							showDialog(DIALOG_SEND_INVALID_EMAIL);							
						}
					} else {
						showDialog(NO_NETWORK_CON);					
					}
				}
			} else {

				 stateCode = getStateCode(stateET.getText()
							.toString(), countryET.getText().toString());

					 state = stateET.getText().toString();

					mCountrySP = countryET.getText().toString();

				if (fName.length() < 1 || lName.length() < 1

						|| mEmail.length() < 1 || state.equals("Select State")
								|| mCountrySP.equals("Select Country")) {
					showDialog(UPDATE_ACCOUNT_All_FIELDS);
				} else {

					stateET.setText(stateET.getText().toString());
					countryET.setText(countryET.getText().toString());

					if (Utility
							.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						if (isEmailValid(mEmail)) {
							loading.setVisibility(View.VISIBLE);
							CWUService
									.getCWUService()
									.sendMyAccountRequest(
											this,
											Utility.user.getId(),
											firstnameET.getText()
													.toString(),
											lastnameET.getText().toString(),
											mEmail, mCountrySP, stateCode,
											Utility.user.getNewsletter());
							// myPd_ring =
							// ProgressDialog.show(MyAccountActivity.this,
							// "Please wait", "Loading please wait..",
							// true);
						} else {
							showDialog(DIALOG_SEND_INVALID_EMAIL);
							// Toast.makeText(this, "HariKrishna",
							// 3000).show();c
						}
					} else {
						showDialog(NO_NETWORK_CON);
						// showDialog("My Account",
						// "No network connection available",
						// 2);
					}
				}

			}
		} else if (v.getId() == R.id.countryETID) {
			stateET.setVisibility(View.GONE);
			countryET.setVisibility(View.GONE);

			stateSP2.setVisibility(View.VISIBLE);
			countrySP1.setVisibility(View.VISIBLE);
			countrySP1.performClick();
		} else if (v.getId() == R.id.stateETID) {

			if (countryET.getVisibility() == View.VISIBLE) {
				Toast.makeText(this, "select country first", 6000).show();
			} else {
				stateET.setVisibility(View.GONE);
				stateSP2.setVisibility(View.VISIBLE);
				stateSP2.performClick();
			}
		}
	}

	private String getStateCode(String st, String country) {
		String state = null;
		int a = 0, b = 0;
		Resources res = getResources();
		String[] str = null;

		for (int i = 0; i < countries.length; i++) {

			if (countries[i].equals(country)) {
				str = res.getStringArray(spinnerRIDS[i]);
				a = i;
				break;
			}
		}
		for (int j = 0; j < str.length; j++) {
			if (str[j].equals(st)) {
				state = stateSPITEMS[a][j];
				break;
			}
		}
		if (state.equals("")) {
			state = "Select State";
		}
		return state;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case UPDATE_ACCOUNT_SUCCUSS:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dailog_layout_my_account_success, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				fieldsDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				fieldsDialog.show();
				break;
			}
			return fieldsDialog;
		} else if (id == 2) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case UPDATE_ACCOUNT_All_FIELDS:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dailog_layout_update_all_man_fields, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				fieldsDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				fieldsDialog.show();
				break;
			}
			return fieldsDialog;
		} else if (id == 3) {
			AlertDialog noNetworkDialog = null;
			switch (id) {
			case NO_NETWORK_CON:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_layout_no_network, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setView(noNetworkView);
				adbNoNet.setCancelable(false);
				noNetworkDialog = adbNoNet.create();
				noNetworkDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noNetworkDialog.show();
				break;
			}
			return noNetworkDialog;
		} else if (id == 4) {
			AlertDialog noNetworkDialog = null;
			switch (id) {
			case MY_ACCOUNT_STRINGS:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dailkog_layout_my_account_strings, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setView(noNetworkView);
				noNetworkDialog = adbNoNet.create();
				noNetworkDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noNetworkDialog.show();
				break;
			}
			return noNetworkDialog;
		} else if (id == 5) {
			AlertDialog dialogDetails2 = null;
			switch (id) {
			case DIALOG_SEND_FAILED:
				LayoutInflater inflater2 = LayoutInflater.from(this);
				View dialogview = inflater2.inflate(
						R.layout.dialog_send_a_frnd_failed, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
						this);
				dialogbuilder.setView(dialogview);
				dialogDetails2 = dialogbuilder.create();
				dialogDetails2.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails2.show();
				break;
			}
			return dialogDetails2;
		} else if (id == 6) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case DIALOG_SEND_INVALID_EMAIL:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dialog_send_a_frnd_invalid, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				fieldsDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				fieldsDialog.show();
				break;
			}
			return fieldsDialog;
		} else if (id == 7) {
			AlertDialog dialogDetails = null;
			switch (id) {
			case LOGOUT:
				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater.inflate(R.layout.dialog_layout_logout,
						null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails.show();
				break;
			}
			return dialogDetails;
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
			switch (id) {
			case UPDATE_ACCOUNT_SUCCUSS:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog1
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				tv11 = (TextView) alertDialog1
						.findViewById(R.id.updateAccountTVID);
				tv11.setTypeface(Utility.font_reg);
				Button okbutton = (Button) alertDialog1
						.findViewById(R.id.updateAccountOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog1.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						startActivity(new Intent(MyAccountActivity.this, DashboardScreenActivity.class));
						MyAccountActivity.this.finish();
						alertDialog1.dismiss();
					}
				});
				break;
			}
		} else if (id == 2) {
			switch (id) {
			case UPDATE_ACCOUNT_All_FIELDS:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				tv11 = (TextView) alertDialog2
						.findViewById(R.id.manFieldAccountTVID);
				tv11.setTypeface(Utility.font_reg);
				Button okbutton = (Button) alertDialog2
						.findViewById(R.id.manFieldAccountOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog2.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
				break;
			}
		} else if (id == 3) {
			switch (id) {
			case NO_NETWORK_CON:
				final AlertDialog alertDialog3 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog3
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				tv12 = (TextView) alertDialog3.findViewById(R.id.noConnTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog3
						.findViewById(R.id.noNetWorkOKID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog3.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog3.dismiss();
					}
				});
				break;
			}
		} else if (id == 4) {
			switch (id) {
			case MY_ACCOUNT_STRINGS:
				final AlertDialog alertDialog4 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog4.findViewById(R.id.alertLogoutTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				tv12 = (TextView) alertDialog4.findViewById(R.id.myAccInvalidTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog4.findViewById(R.id.myAccinValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog4.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						startActivity(new Intent(MyAccountActivity.this, DashboardScreenActivity.class));
						MyAccountActivity.this.finish();
						alertDialog4.dismiss();
					}
				});
				break;
			}
		} else if (id == 5) {
			final AlertDialog alertDialog2 = (AlertDialog) dialog;
			textTV = (TextView) alertDialog2.findViewById(R.id.failedSentTVID);
			textTV.setTypeface(Utility.font_reg);
			textTV2 = (TextView) alertDialog2.findViewById(R.id.alertSucTitleTVID);
			textTV2.setTypeface(Utility.font_bold);
			textTV2.setText("Fail !");
			textTV.setText("Update failed");
			okbutton = (Button) alertDialog2.findViewById(R.id.sendFailedOkBtnID);
			okbutton.setTypeface(Utility.font_bold);
			alertDialog2.setCancelable(false);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog2.dismiss();
				}
			});
		} else if (id == 6) {
			final AlertDialog alertDialog1 = (AlertDialog) dialog;

			TextView alertTitle = (TextView) alertDialog1
					.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
			alertTitle.setTypeface(Utility.font_bold);
			textTV = (TextView) alertDialog1
					.findViewById(R.id.inValidFieldTVID);
			textTV.setTypeface(Utility.font_reg);
			Button okbutton = (Button) alertDialog1
					.findViewById(R.id.inValidOKBtnID);
			okbutton.setTypeface(Utility.font_bold);
			alertDialog1.setCancelable(false);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog1.dismiss();
				}
			});
		} else if (id == 7) {
			switch (id) {
			case LOGOUT:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView) alertDialog1
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				logoutText = (TextView) alertDialog1
						.findViewById(R.id.my_logoutTVID);
				logoutText.setTypeface(Utility.font_reg);
				loginbutton1 = (Button) alertDialog1
						.findViewById(R.id.loginBtnH_ID);
				loginbutton1.setTypeface(Utility.font_bold);
				cancelbutton1 = (Button) alertDialog1
						.findViewById(R.id.btn_cancelH_ID);
				cancelbutton1.setTypeface(Utility.font_bold);

				loginbutton1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						try {
							helper.deleteLoginDetails();
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
							}
						}
						Intent logoutIntent = new Intent(MyAccountActivity.this, LoginScreenActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP);
						startActivity(logoutIntent);
						LoginScreenActivity.etUserId.setText("");
						LoginScreenActivity.etPasswd.setText("");
						MyAccountActivity.this.finish();
						alertDialog1.dismiss();
					}
				});
				cancelbutton1.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = true;
		if (v.getId() == R.id.firstnameETID) {
			if (firstnameET.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.lastnameETID) {
			if (emailET.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		} else if (v.getId() == R.id.emailETID) {
			if (lastnameET.getText().length() > 25) {
				returnValue = true;

			} else {
				returnValue = false;
			}
		}
		return returnValue;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(MyAccountActivity.this, DashboardScreenActivity.class));
			MyAccountActivity.this.finish();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
}
