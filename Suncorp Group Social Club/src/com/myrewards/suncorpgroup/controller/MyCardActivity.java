package com.myrewards.suncorpgroup.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myrewards.suncorpgroup.cache.SmartImageView;
import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.utils.ApplicationConstants;
import com.myrewards.suncorpgroup.utils.Utility;

@SuppressLint("CutPasteId")
@SuppressWarnings("deprecation")
public class MyCardActivity extends Activity implements
		CWUServiceListener, OnClickListener {
	// ProgressDialog myPd_ring;
	View loading;
	Button backButton, scanBarBtn;
	TextView titleTV, cardIsLoadingTV;
	TextView tv12;
	Button okbutton;
	TextView alertTilteTv, alertMsgTV;
	final private static int NO_NETWORK_CON = 1;
	private TextView clientNameTV, cardNameTV, cardMNoTV;
	SmartImageView cardIV;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_card);
		
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		
		titleTV=(TextView)findViewById(R.id.titleTVID);
		titleTV.setText(getResources().getString(R.string.my_card));
		titleTV.setTypeface(Utility.font_bold);
		scanBarBtn=(Button)findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);	
		
		loading = (View) findViewById(R.id.loading);
		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backButton.setOnClickListener(this);

		clientNameTV = (TextView) findViewById(R.id.clientNameTVID);
		clientNameTV.setTypeface(Utility.font_bold);

		cardNameTV = (TextView) findViewById(R.id.cardNameTVID);
		cardNameTV.setTypeface(Utility.font_bold);

		cardMNoTV = (TextView) findViewById(R.id.cardMNoTVID);
		cardMNoTV.setTypeface(Utility.font_bold);
		
		cardIsLoadingTV=(TextView)findViewById(R.id.cardLoadTVID);
		cardIsLoadingTV.setTypeface(Utility.font_bold);
		
		cardIV = (SmartImageView) findViewById(R.id.cardRLID);
		cardIV.getLayoutParams().width = 4 * (Utility.screenWidth / 6);
		cardIV.getLayoutParams().height = (int) ((1.3) * cardIV.getLayoutParams().width);

		
		try {
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				if (Utility.user != null) {
					if (Utility.user.getMyMembershipCard() == null) {
						String myMembershipCard = "MemberShipCard";
						String cardUrl = ApplicationConstants.MY_CARD_URL_IMAGE + Utility.user.getClient_id()+"."+ Utility.user.getCard_ext();
						newCardImagesLoading(cardUrl, myMembershipCard);
					}
				}
			} else {
				loading.setVisibility(View.GONE);
				showDialog(NO_NETWORK_CON);
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			
			}
		}
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {

	}

	
	
	private void newCardImagesLoading(String cardURL, String myMembershipCard) {
		// Image url
		String image_url = cardURL;
		Log.w("Hari-->", cardURL);

		// ImageLoader class instance
	//	MyImageLoader imgLoader = new MyImageLoader(getApplicationContext(), myMembershipCard);

		// whenever you want to load an image from url
		// call DisplayImage function
		// url - image url to load
		// loader - loader image, will be displayed before getting image
		// image - ImageView
		try {
			//imgLoader.DisplayImage(image_url, cardIV);
			cardIV.setImageUrl(image_url);
			} catch (OutOfMemoryError e) {
				if ( e != null) {
					Log.w("Hari-->DEBUG", e);
			}
		}

		try {
			clientNameTV.setText("Client: " + Utility.user.getClient_name());
			cardNameTV.setText("Name: " + Utility.user.getFirst_name() + " " + Utility.user.getLast_name());
			cardMNoTV.setText("Membership: " + Utility.user.getUsername());
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			
			}
		}
		loading.setVisibility(View.GONE);
	}

	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id) {
			AlertDialog noNetworkDialog = null;
			switch (id) {
			case NO_NETWORK_CON:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_layout_no_network, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setCancelable(false);
				adbNoNet.setView(noNetworkView);
				noNetworkDialog = adbNoNet.create();
				noNetworkDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noNetworkDialog.show();
				break;
			}
			return noNetworkDialog;
	}

	@Override
	@Deprecated
	protected void onPrepareDialog(int id, Dialog dialog) {
			switch (id) {
			case NO_NETWORK_CON:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				alertTilteTv = (TextView) alertDialog2.findViewById(R.id.alertLogoutTitleTVID);
				alertTilteTv.setTypeface(Utility.font_bold);
				alertMsgTV = (TextView) alertDialog2.findViewById(R.id.noConnTVID);
				alertMsgTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2.findViewById(R.id.noNetWorkOKID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						startActivity(new Intent(MyCardActivity.this, DashboardScreenActivity.class));
						MyCardActivity.this.finish();
						alertDialog2.dismiss();
					}
				});
				break;
		}
		super.onPrepareDialog(id, dialog);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backBtnID) {
			startActivity(new Intent(MyCardActivity.this, DashboardScreenActivity.class));
			MyCardActivity.this.finish();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(MyCardActivity.this, DashboardScreenActivity.class));
			MyCardActivity.this.finish();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
}
