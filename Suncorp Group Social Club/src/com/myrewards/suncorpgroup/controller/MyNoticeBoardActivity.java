package com.myrewards.suncorpgroup.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.myrewards.suncorpgroup.model.NoticeBoard;
import com.myrewards.suncorpgroup.model.NoticeId;
import com.myrewards.suncorpgroup.model.ProductNotice;
import com.myrewards.suncorpgroup.service.CWUService;
import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.utils.DatabaseHelper;
import com.myrewards.suncorpgroup.utils.Utility;

@SuppressWarnings("deprecation")
public class MyNoticeBoardActivity extends Activity implements
		CWUServiceListener, OnItemClickListener, OnClickListener {
	List<ProductNotice> productsList;
	ProductNotice productNotice;
	List<NoticeBoard> noticeBoardProductsList;
	LayoutInflater inflater;
	public static boolean noticecount = false;
	ListView noticeboardListView;
	NoticeboardAdapter mAdapter;
	DatabaseHelper helper;
	View loading;
	Button backBtn, scanBarBtn;
	TextView titleTV;
	ImageView noticeArrow;
	TextView productNameTV, createdTitleTV;
	ImageView noticeImage;

	// show dialog...........
	final private static int NO_NETWORK_CON = 1;
	public TextView tv12;
	public Button okbutton;
	List<NoticeId> noticeid;
	ToggleButton tButton;
	String header;
	String form_tabsRDashboard = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_notice_board);
		//setMyNoticesList();
	}	
	
	@Override
	protected void onResume() {
		super.onResume();
		setMyNoticesList();
	}
	
	private void setMyNoticesList() {
		helper = new DatabaseHelper(this);
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.social_club_news_title));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		loading = (View) findViewById(R.id.loading);
		backBtn = (Button) findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setOnClickListener(this);
		
		headerImage.setVisibility(View.GONE);
		titleTV.setVisibility(View.GONE);
		backBtn.setVisibility(View.GONE);
		
		RelativeLayout noticeUpdateRL = (RelativeLayout) findViewById(R.id.noticeUpdateRLID);

		TextView noticeUpdateText = (TextView) findViewById(R.id.noticeUpdateTVID);
		noticeUpdateText.setTypeface(Utility.font_bold);
		
		tButton = (ToggleButton) findViewById(R.id.toggleButton1);
		//tButton.getLayoutParams().width = (int) (Utility.screenWidth / 7);
		tButton.getLayoutParams().height = (int) (Utility.screenHeight / 15.0);
		
		if (getIntent() !=null) {
			Bundle b= getIntent().getExtras();
			if (b !=null) {
				header = b.getString(Utility.readNoticesEXTRA);
			}
		}
		
		if (header !=null) {
			if (header.equals("READ_HEADER")) {
				headerImage.setVisibility(View.VISIBLE);
				titleTV.setVisibility(View.VISIBLE);
				backBtn.setVisibility(View.VISIBLE);
				noticeUpdateRL.setVisibility(View.VISIBLE);
				form_tabsRDashboard = "FROM_DASHBOARD";
			} else {
				form_tabsRDashboard = "FROM_GIN_TABS";
			}
		} else {
			form_tabsRDashboard = "FROM_GIN_TABS";
		}
		
		if (helper.getNoticeUpdateState().equals("ON")){
			tButton.setChecked(true);
		}
		else {
			tButton.setChecked(false);
		}
		tButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				try {
					if (isChecked) {
						helper.setNoticeUpdateState("ON");
						Utility.setNotificationReceiver(MyNoticeBoardActivity.this);
					} else {
						helper.setNoticeUpdateState("OFF");
						Utility.cancelNotificationReceiver();
					}
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
					}
				}
			}
		});

		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			loading.setVisibility(View.VISIBLE);
			CWUService.getCWUService().sendMyNoticeBoardRequest(this);
		} else {
			showDialog(NO_NETWORK_CON);
		}
		noticeboardListView = (ListView) findViewById(R.id.noticeboardListViewID);
		noticeboardListView.setOnItemClickListener(this);		
	}

	

	public class NoticeboardAdapter extends BaseAdapter {

		public NoticeboardAdapter(MyNoticeBoardActivity myNoticeBoardActivity) {

		}

		@Override
		public int getCount() {
			return productsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {

			View row = null;
			if (row == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = (View) inflater.inflate(R.layout.noticeboard_list_item,
						null, false);
			}
			productNameTV = (TextView) row.findViewById(R.id.productNoticeTVID);
			productNameTV.setSelected(true);
			productNameTV.setTypeface(Utility.font_bold);

			RelativeLayout rowLL2 = (RelativeLayout) row.findViewById(R.id.resultItemNotice5RLID);
			rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 9.4);

			noticeImage = (ImageView) row.findViewById(R.id.imageView1);
			noticeArrow = (ImageView) row.findViewById(R.id.arrowIVID);
			productNameTV.setText(productsList.get(pos).getSubject());
			RelativeLayout rowRL = (RelativeLayout) row.findViewById(R.id.resultItemNoticeRLID);
			switch (pos % 4) {
			case 0:
				rowRL.setBackgroundResource(R.color.result_color_one_main);
				productNameTV.setTextColor(Color.parseColor("#9CEAE3"));
				break;
			case 1:
				rowRL.setBackgroundResource(R.color.result_color_two_main);
				productNameTV.setTextColor(Color.parseColor("#359A8D"));
				break;
			case 2:
				rowRL.setBackgroundResource(R.color.result_color_three_main);
				productNameTV.setTextColor(Color.parseColor("#9CEAE3"));
				break;
			case 3:
				rowRL.setBackgroundResource(R.color.result_color_four_main);
				productNameTV.setTextColor(Color.parseColor("#359A8D"));
				break;
			}
			try {
				if (helper.getNoticeIdReadorUnread(productsList.get(pos).getId()).equals("CLOSE")) {
					noticeImage.setImageResource(R.drawable.notice_read);
				} else if (helper.getNoticeIdReadorUnread(productsList.get(pos).getId()).equals("OPEN")) {
					noticeImage.setImageResource(R.drawable.notice_unread);
				} 
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					
				}				
			}
			return row;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (response != null) {
				if (response instanceof String) {
				} else {
					if (eventType == 10) {
						productsList = (List<ProductNotice>) response;
						mAdapter = new NoticeboardAdapter(this);
						noticeboardListView.setAdapter(mAdapter);
					}
				}
			}
			updateNoticeBoardList();
		
		} catch (Exception e) {
			if (e !=null) {
				Log.v("HHHHHHHHH", e.getMessage());
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		try {
			noticecount = true;
			helper = new DatabaseHelper(this);
			helper.updateNoticeDetails(productsList.get(pos).getId());
			ProductNotice tappedItem = productsList.get(pos);
			Intent intent = new Intent(MyNoticeBoardActivity.this, MyNoticeBoardDetailsActivity.class);
			intent.putExtra(MyNoticeBoardDetailsActivity.DETAILS, tappedItem);
			if (form_tabsRDashboard !=null) {
				if (form_tabsRDashboard.equals("FROM_GIN_TABS")) {
					intent.putExtra(Utility.FROM_GIN_TABS, "FROM_GIN_TABS_Notice");
				} else if (form_tabsRDashboard.equals("FROM_DASHBOARD")) {
					intent.putExtra(Utility.FROM_DASHBOARD, "FROM_DASHBOARD_Notice");
				}
			}
			startActivity(intent);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
		}
	}

	// custom dialog..........
	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog noNetworkDialog = null;
			switch (id) {
			case NO_NETWORK_CON:
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_layout_no_network, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setCancelable(false);
				adbNoNet.setView(noNetworkView);
				noNetworkDialog = adbNoNet.create();
				noNetworkDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noNetworkDialog.show();
				break;
			}
			return noNetworkDialog;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
			switch (id) {
			case NO_NETWORK_CON:
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				TextView alertMsgTV = (TextView) alertDialog2
						.findViewById(R.id.noConnTVID);
				alertMsgTV.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2
						.findViewById(R.id.noNetWorkOKID);
				okbutton.setTypeface(Utility.font_bold);
				alertDialog2.setCancelable(false);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						startActivity(new Intent(MyNoticeBoardActivity.this, DashboardScreenActivity.class));
						MyNoticeBoardActivity.this.finish();
						alertDialog2.dismiss();
					}
				});
				break;
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backBtnID:
			try {
				updateNoticeBoardList();
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardActivity.this.finish();
			}
			break;
		}
	}

	private void updateNoticeBoardList() {
		noticeid = new ArrayList<NoticeId>();
		for (int i = 0; i < productsList.size(); i++) {
			noticeid.add(new NoticeId(productsList.get(i).getId()));
		}
		if (noticeid != null)
		{
			List<String> shambhiList = new ArrayList<String>();
			for (int n = 0; n < noticeid.size(); n++) {
				if (!(noticeid.get(n).getNoticeDetails() == null))
					shambhiList.add(noticeid.get(n).getNoticeDetails());
			}
			if (shambhiList.size() != helper.getExistingIDs().size()) {
				for (int m = 0; m < helper.getExistingIDs().size(); m++) {
					if (shambhiList.contains(helper.getExistingNoticeRealIDs().get(m))) {
					} else {
						helper.deleteNoticeIdDetails(helper.getExistingNoticeRealIDs().get(m));
					}
				}
			}
			for (int k = 0; k < noticeid.size(); k++) {
				if (noticeid.get(k).getNoticeDetails() != null) {

					List<String> list = helper.getExistingIDs();
					if (!list.contains(noticeid.get(k).getNoticeDetails())) {
						helper.addnoticeiddetails(noticeid.get(k)
								.getNoticeDetails());
					}
				}
			}
		}
		loading.setVisibility(View.GONE);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			try {
				updateNoticeBoardList();
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardActivity.this.finish();
			} catch (Exception e) {
				startActivity(new Intent(this, DashboardScreenActivity.class));
				MyNoticeBoardActivity.this.finish();
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}
