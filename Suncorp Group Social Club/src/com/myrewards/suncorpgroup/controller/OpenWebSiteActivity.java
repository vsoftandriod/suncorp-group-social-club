package com.myrewards.suncorpgroup.controller;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

@SuppressLint("SetJavaScriptEnabled")
public class OpenWebSiteActivity extends Activity {
	View loading;
	WebView webView;
	//Button backButton;
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.web_site_my_union_contacts);
	webView=(WebView)findViewById(R.id.webview);
	//backButton.setOnClickListener(this);
	webView.getSettings().setJavaScriptEnabled(true);
	webView.getSettings().setAllowFileAccess(true);
	webView.getSettings().setLoadsImagesAutomatically(true);
	
	WebSettings settings = webView.getSettings();
	settings.setDefaultTextEncodingName("utf-8");
	
	loading=(View)findViewById(R.id.loading);
	loading.setVisibility(View.GONE);
	
	webView.loadUrl(getResources().getString(R.string.website));
  }
}
