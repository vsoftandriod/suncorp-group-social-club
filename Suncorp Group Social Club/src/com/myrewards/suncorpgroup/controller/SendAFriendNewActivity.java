package com.myrewards.suncorpgroup.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.suncorpgroup.model.User;
import com.myrewards.suncorpgroup.service.CWUServiceListener;
import com.myrewards.suncorpgroup.service.GrabItNowService;
import com.myrewards.suncorpgroup.utils.DatabaseHelper;
import com.myrewards.suncorpgroup.utils.Utility;

@SuppressWarnings("deprecation")
public class SendAFriendNewActivity extends Activity implements
		OnClickListener, CWUServiceListener, OnLongClickListener {
	Button sendBtn11;
	Button backBtn, sendBtn, scanBarBtn;
	TextView titleTV;
	LayoutInflater inflater;
	EditText mUname, mEmail;
	TextView alertTilteTv, alertMsgTV;
	DatabaseHelper helper;
	ListView emailList;
	private static final int DIALOG_SEND = 1;
	private static final int DIALOG_SEND_SUCCESS = 2;
	private static final int DIALOG_SEND_FAILED = 3;
	private static final int DIALOG_SEND_FIELDS_ERROR = 4;
	private static final int DIALOG_SEND_INVALID_EMAIL = 5;
	User user;
	Button okbutton;

	// AlertDialog alertDialogCustom = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_a_friend);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(getResources().getString(R.string.send_a_friend));

		scanBarBtn = (Button) findViewById(R.id.scanBtnID);
		scanBarBtn.setVisibility(View.GONE);

		helper = new DatabaseHelper(this);
		user = new User();
		backBtn = (Button) findViewById(R.id.backBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(SendAFriendNewActivity.this, DashboardScreenActivity.class));
				SendAFriendNewActivity.this.finish();
			}
		});
		sendBtn11 = (Button) findViewById(R.id.sendafrndbtnID);
		sendBtn11.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		showDialog(DIALOG_SEND);
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (response != null) {
				if (response.toString().contains("SUCCESS")
						|| response.toString().contains("success")) {
					showDialog(DIALOG_SEND_SUCCESS);
				} else {
					showDialog(DIALOG_SEND_FAILED);
				}
			}
		
		} catch (Exception e) {
			
			Toast.makeText(getApplicationContext(), "Server busy. Please wait or try again.", Toast.LENGTH_LONG).show();
			if (e != null) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog sendDetails = null;
			switch (id) {
			case DIALOG_SEND:
				LayoutInflater inflateSend = LayoutInflater.from(this);
				View deleteFavView = inflateSend.inflate(
						R.layout.dialog_layout_send_emails_frnds, null);
				AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(this);
				adbDeleteFav.setView(deleteFavView);
				sendDetails = adbDeleteFav.create();
				sendDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				sendDetails.show();
				break;
			}
			return sendDetails;
		} else if (id == 2) {
			AlertDialog dialogDetails2 = null;
			switch (id) {
			case DIALOG_SEND_SUCCESS:
				LayoutInflater inflater2 = LayoutInflater.from(this);
				View dialogview = inflater2.inflate(
						R.layout.dialog_send_a_frnd_success, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
						this);
				dialogbuilder.setView(dialogview);
				dialogDetails2 = dialogbuilder.create();
				dialogDetails2.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails2.show();
				break;
			}
			return dialogDetails2;
		} else if (id == 3) {
			AlertDialog dialogDetails2 = null;
			switch (id) {
			case DIALOG_SEND_FAILED:
				LayoutInflater inflater2 = LayoutInflater.from(this);
				View dialogview = inflater2.inflate(
						R.layout.dialog_send_a_frnd_failed, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
						this);
				dialogbuilder.setView(dialogview);
				dialogDetails2 = dialogbuilder.create();
				dialogDetails2.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails2.show();
				break;
			}
			return dialogDetails2;
		} else if (id == 4) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case DIALOG_SEND_FIELDS_ERROR:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.alert_first_login_all_fields_error, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				fieldsDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				fieldsDialog.show();
				break;
			}
			return fieldsDialog;
		} else if (id == 5) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case DIALOG_SEND_INVALID_EMAIL:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(
						R.layout.dialog_send_a_frnd_invalid, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				fieldsDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				fieldsDialog.show();
				break;
			}
			return fieldsDialog;
		}
		return super.onCreateDialog(id);
	}

	@SuppressLint("ShowToast")
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
			final AlertDialog alertDialogCustom = (AlertDialog) dialog;
			alertTilteTv = (TextView) alertDialogCustom
					.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			mUname = (EditText) alertDialogCustom.findViewById(R.id.nameId);
			mUname.setTypeface(Utility.font_reg);
			mUname.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
			mEmail = (EditText) alertDialogCustom.findViewById(R.id.emailId);
			mEmail.setTypeface(Utility.font_reg);
			mEmail.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);

			mUname.setOnLongClickListener(this);
			mEmail.setOnLongClickListener(this);
			
			final InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			im.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
			mEmail.setOnClickListener(new OnClickListener() {

		        @Override
		        public void onClick(View v) {
		            // TODO Auto-generated method stub
		        im.showSoftInput(mEmail, InputMethodManager.SHOW_IMPLICIT);
		        }
		    });
			
			final InputMethodManager im2 = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			im2.hideSoftInputFromWindow(mUname.getWindowToken(), 0);
			mUname.setOnClickListener(new OnClickListener() {

		        @Override
		        public void onClick(View v) {
		            // TODO Auto-generated method stub
		        im2.showSoftInput(mUname, InputMethodManager.SHOW_IMPLICIT);
		        }
		    });

			Button sendBtn = (Button) alertDialogCustom
					.findViewById(R.id.sendBtnID);
			sendBtn.setTypeface(Utility.font_bold);
			Button sendCloseBtn = (Button) alertDialogCustom
					.findViewById(R.id.sendCloseBtnID);
			sendCloseBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					im.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
					im2.hideSoftInputFromWindow(mUname.getWindowToken(), 0);
					alertDialogCustom.dismiss();
				}
			});
			sendBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					String frndName = mUname.getText().toString();
					String frndEmailID = mEmail.getText().toString();
					if (frndName != null && frndName.trim().length() > 0) {
						if (isEmailValid(frndEmailID)) {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								GrabItNowService.getGrabItNowService()
								.sendSendAFriendRequest(
										SendAFriendNewActivity.this,
										Utility.user.getId(),
										mUname.getText().toString(),
										mEmail.getText().toString());
							} else {
								alertDialogCustom.dismiss();
								// The Custom Toast Layout Imported here
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								//layout.getBackground().setAlpha(128);  // 50% transparent
								 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} else {
							showDialog(DIALOG_SEND_INVALID_EMAIL);
						}

					} else {
						showDialog(DIALOG_SEND_FIELDS_ERROR);
					}
					im.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
					im2.hideSoftInputFromWindow(mUname.getWindowToken(), 0);
				}
			});
		} else if (id == 2) {
			final AlertDialog alertDialogCustom = (AlertDialog) dialog;
			alertTilteTv = (TextView) alertDialogCustom
					.findViewById(R.id.firstLoginCEPUTitleTVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			alertMsgTV = (TextView) alertDialogCustom
					.findViewById(R.id.succSentTVID);
			alertMsgTV.setTypeface(Utility.font_reg);
			okbutton = (Button) alertDialogCustom
					.findViewById(R.id.sendSuccOkBtnID);
			okbutton.setTypeface(Utility.font_bold);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(new Intent(SendAFriendNewActivity.this,DashboardScreenActivity.class));
					SendAFriendNewActivity.this.finish();
					alertDialogCustom.dismiss();
				}
			});
		} else if (id == 3) {
			final AlertDialog alertDialogCustom = (AlertDialog) dialog;
			alertTilteTv = (TextView) alertDialogCustom
					.findViewById(R.id.firstLoginCEPUimgTitleIVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			alertMsgTV = (TextView) alertDialogCustom
					.findViewById(R.id.failedSentTVID);
			alertMsgTV.setTypeface(Utility.font_reg);
			okbutton = (Button) alertDialogCustom
					.findViewById(R.id.sendFailedOkBtnID);
			okbutton.setTypeface(Utility.font_bold);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(new Intent(SendAFriendNewActivity.this,DashboardScreenActivity.class));
					SendAFriendNewActivity.this.finish();
					alertDialogCustom.dismiss();
				}
			});
		} else if (id == 4) {
			final AlertDialog alertDialogCustom = (AlertDialog) dialog;
			alertTilteTv = (TextView) alertDialogCustom
					.findViewById(R.id.allFieldsTVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			alertMsgTV = (TextView) alertDialogCustom
					.findViewById(R.id.allFieldsMessageTVID);
			alertMsgTV.setTypeface(Utility.font_reg);
			okbutton = (Button) alertDialogCustom
					.findViewById(R.id.allFieldsOKBtnID);
			okbutton.setTypeface(Utility.font_bold);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialogCustom.dismiss();
				}
			});
		} else if (id == 5) {
			final AlertDialog alertDialogCustom = (AlertDialog) dialog;
			alertTilteTv = (TextView) alertDialogCustom
					.findViewById(R.id.emailInValidTitleTVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			alertMsgTV = (TextView) alertDialogCustom
					.findViewById(R.id.inValidEmailFieldTVID);
			alertMsgTV.setTypeface(Utility.font_reg);
			okbutton = (Button) alertDialogCustom
					.findViewById(R.id.inValidOEmailKBtnID);
			okbutton.setTypeface(Utility.font_bold);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialogCustom.dismiss();
				}
			});
		}
	}

	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = true;
		if (v.getId() == R.id.emailETID) {
			if (mEmail.getText().length() > 60) {
				returnValue = true;
			} else {
				returnValue = false;
			}
		}
		return returnValue;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(SendAFriendNewActivity.this,
					DashboardScreenActivity.class));
			SendAFriendNewActivity.this.finish();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

}
