package com.myrewards.suncorpgroup.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myrewards.suncorpgroup.controller.R;
import com.myrewards.suncorpgroup.utils.Utility;

public class HelpPageFour extends Fragment {
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v=(LinearLayout)inflater.inflate(R.layout.page4_frag4_layout, container, false);
		TextView textview1=(TextView)v.findViewById(R.id.page1_IVID);
		textview1.setMovementMethod(new ScrollingMovementMethod());
		textview1.setTypeface(Utility.font_reg);
		//textview1.setText(Html.fromHtml("<h4> Search Results wherever you are... </h4>  &#9733;Your search results will be retrieved.<br> <br>&#9733; If you don't get a result , widen your <br>&nbsp&nbsp&nbsp&nbsp search parameters.<br> <br>&#9733; You will receive a list of offers that <br>&nbsp&nbsp&nbsp&nbsp   meet the search criteria <br><br>&#9733; Click on your preferred supplier in the <br>&nbsp&nbsp&nbsp&nbsp  search results <br> <br>&#9733; This will take type the offer page <br>&nbsp&nbsp&nbsp&nbsp  of that merchant <br><br><br><br><br><br><br><br><br><br><br>"));
		textview1.setText(Html.fromHtml("<h4> Search Results wherever you are... </h4>  &#9733;Your search results will be retrieved.<br> <br>&#9733; If you don't get a result , widen your search parameters.<br> <br>&#9733; You will receive a list of offers that meet the search criteria <br><br>&#9733; Click on your preferred supplier in the search results <br> <br>&#9733; This will take type the offer page of that merchant <br><br><br><br><br><br><br>"));
		return v;
		}
}