package com.myrewards.suncorpgroup.model;

public class Product {
	private int id;
	private String name;
	private String highlight;
	private String latitude;
	private String longitude;
	private String hotoffer_extension;
	private String pinType;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHighlight() {
		return highlight;
	}
	public void setHighlight(String highlight) {
		this.highlight = highlight;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getHotoffer_extension() {
		return hotoffer_extension;
	}
	public void setHotoffer_extension(String hotoffer_extension) {
		this.hotoffer_extension = hotoffer_extension;
	}
	public String getPinType() {
		return pinType;
	}
	public void setPinType(String pinType) {
		this.pinType = pinType;
	}
}
