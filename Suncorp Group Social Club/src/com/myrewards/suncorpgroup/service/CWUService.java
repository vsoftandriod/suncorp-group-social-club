package com.myrewards.suncorpgroup.service;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.myrewards.suncorpgroup.model.ProductNotice;
import com.myrewards.suncorpgroup.model.User;
import com.myrewards.suncorpgroup.utils.ApplicationConstants;
import com.myrewards.suncorpgroup.utils.Utility;
import com.myrewards.suncorpgroup.xml.NoticeBoardParser;
import com.myrewards.suncorpgroup.xml.UserDetailsParser;

public class CWUService implements CWUNetworkListener {
	private static final int REQUEST_LOGIN = 1;
	private static final int REQUEST_USER_DETAILS = 2;
	// private static final int REQUEST_CLIENT_BANNER = 3;
	private static final int REQUEST_CATEGORY_LIST = 4;
	private static final int REQUEST_HOT_OFFERS = 5;
	private static final int REQUEST_SEARCH_PRODUCTS = 6;
	private static final int REQUEST_PRODUCTS_DETAILS = 7;
	private static final int REQUEST_TEST = 8;
	private static final int REQUEST_MY_ACCOUNT = 9;
	private static final int REQUEST_MY_NOTICE = 10;

	private CWUServiceListener cWUServiceListener;
	private static CWUService myRewardsService;
	private String username;

	public static CWUService getCWUService() {
		if (myRewardsService == null) {
			myRewardsService = new CWUService();
		}
		return myRewardsService;
	}

	public void sendTestRequestAbout(CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(ApplicationConstants.ABOUT_US_WRAPPER, null, null, this, REQUEST_TEST);
		//String l = ApplicationConstants.ABOUT_US_WRAPPER;
		//String link = Html.fromHtml("<a href=\""+ l.getRightString() + "\">" + l.getLeftString() + "</a>";
	}

	public void sendTestRequestDeals(CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.MY_DEAL_WRAPPER, null, null, this,
				REQUEST_TEST);
	}

	public void sendLoginRequest(CWUServiceListener cWUServiceListener,
			String username, String password, String affiliateId) {
		this.cWUServiceListener = cWUServiceListener;
		this.username = username;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.LOGIN_WRAPPER,
				getLoginRequestData(username, password, affiliateId), null,
				this, REQUEST_LOGIN);
	}

	private String getLoginRequestData(String username, String password,
			String subDomainURL) {
		String loginXML = "uname=" + username + "&pwd=" + password + "&sub="
				+ subDomainURL;
		return loginXML;
	}

	private void sendUserDetailsRequest() {
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.USER_DETAILS_WRAPPER + username, null,
				null, this, REQUEST_USER_DETAILS);
	}

	public void sendCategoriesListRequest(CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.CATEGORY_WRAPPER + "&cid="
						+ Utility.user.getClient_id() + "&country="
						+ Utility.user.getCountry(), null, null, this,
				REQUEST_CATEGORY_LIST);
	}

	public void sendHotOffersRequest(CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.HOT_OFFERS_WRAPPER
						+ Utility.user.getClient_id(), null, null, this,
				REQUEST_HOT_OFFERS);
	}

	public void sendSearchProductsRequest(
			CWUServiceListener cWUServiceListener, String catID,
			String location, String keyword) {
		this.cWUServiceListener = cWUServiceListener;
		String queryString = "cid=" + Utility.user.getClient_id();
		if (catID != null) {
			queryString = queryString + "&cat_id=" + catID;
		}
		if (location.length() > 0) {
			queryString = queryString + "&p=" + location;
		}
		if (keyword.length() > 0) {
			queryString = queryString + "&q=" + keyword;
		}
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.SEARCH_PRODUCTS_WRAPPER + queryString
						+ "&country=" + Utility.user.getCountry()
						+ "&start=0&limit=30", null, null, this,
				REQUEST_SEARCH_PRODUCTS);
	}

	public void sendProductDetailsRequest(
			CWUServiceListener cWUServiceListener, int productId) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler()
				.sendRequestAsync(
						ApplicationConstants.PRODUCT_DETAILS_WRAPPER + "id="
								+ productId, null, null, this,
						REQUEST_PRODUCTS_DETAILS);
	}

	public void sendMyAccountRequest(CWUServiceListener bayfordServiceListener,
			int username, String firstName, String lastName, String email,
			String country, String state, String newsLetter) {
		this.cWUServiceListener = bayfordServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.MY_ACCOUNT_WRAPPER,
				getMyAccountRequestData(username, firstName, lastName, email,
						country, state, newsLetter), null, this,
				REQUEST_MY_ACCOUNT);
	}

	private String getMyAccountRequestData(int username, String firstName,
			String lastName, String email, String country, String state,
			String newsLetter) {
		String requestData = "uname=" + username + "&fname=" + firstName
				+ "&lname=" + lastName + "&email=" + email + "&country="
				+ country + "&state=" + state + "&newsletter=" + newsLetter;
		return requestData;
	}

	public void sendMyNoticeBoardRequest(CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(ApplicationConstants.NOTICE_BOARD_WRAPPER_DAILY_SAVERS, null, null, this, REQUEST_MY_NOTICE);
	}

	public void onRequestCompleted(String response, String errorString, int eventType) {
		switch (eventType) {
		case REQUEST_LOGIN:
			if (errorString == null && response != null) {
				try {
					DocumentBuilderFactory factory = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder db = factory.newDocumentBuilder();
					InputSource inStream = new InputSource();
					inStream.setCharacterStream(new StringReader(response));
					Document doc = db.parse(inStream);

					String message = "status";
					NodeList messageId_nl = doc.getElementsByTagName("status");
					for (int i = 0; i < messageId_nl.getLength(); i++) {
						if (messageId_nl.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
							org.w3c.dom.Element nameElement = (org.w3c.dom.Element) messageId_nl
									.item(i);
							message = nameElement.getFirstChild()
									.getNodeValue().trim();
						}
					}
					if (message != null && message.equalsIgnoreCase("SUCCESS")) {
						sendUserDetailsRequest();
					} else
						cWUServiceListener
								.onServiceComplete(message, eventType);

				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
			}
			break;
		case REQUEST_USER_DETAILS:
			if (errorString == null && response != null) {
				try {
					User user = new User();
					new UserDetailsParser().internalXMLParse(response, user);
					cWUServiceListener.onServiceComplete(user, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
			}
			break;
		case REQUEST_CATEGORY_LIST:
			break;
		case REQUEST_HOT_OFFERS:
			break;
		case REQUEST_SEARCH_PRODUCTS:
			break;
		case REQUEST_PRODUCTS_DETAILS:
			break;
		case REQUEST_MY_ACCOUNT:
			if (errorString == null && response != null) {
				try {
					cWUServiceListener.onServiceComplete(response, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
			}
			break;
		case REQUEST_MY_NOTICE:
			if (errorString == null && response != null) {
				try {
					List<ProductNotice> noticeBoardList = new ArrayList<ProductNotice>();
					new NoticeBoardParser().internalXMLParse(response,
							noticeBoardList);
					cWUServiceListener.onServiceComplete(noticeBoardList,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
			}
			break;
		case REQUEST_TEST:
			if (errorString == null && response != null) {
				try {

					cWUServiceListener.onServiceComplete(response, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener.onServiceComplete(ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL, eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
			}
			break;
		}
	}
}
