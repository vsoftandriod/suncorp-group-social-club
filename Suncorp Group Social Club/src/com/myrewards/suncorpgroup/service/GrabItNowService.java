package com.myrewards.suncorpgroup.service;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.myrewards.suncorpgroup.model.Category;
import com.myrewards.suncorpgroup.model.NoticeBoard;
import com.myrewards.suncorpgroup.model.NoticeId;
import com.myrewards.suncorpgroup.model.Product;
import com.myrewards.suncorpgroup.model.ProductAddress;
import com.myrewards.suncorpgroup.model.ProductDetails;
import com.myrewards.suncorpgroup.model.User;
import com.myrewards.suncorpgroup.utils.ApplicationConstants;
import com.myrewards.suncorpgroup.utils.Utility;
import com.myrewards.suncorpgroup.xml.CategoryListParser;
import com.myrewards.suncorpgroup.xml.DailyDealsParser;
import com.myrewards.suncorpgroup.xml.FirsttimeLoginParser;
import com.myrewards.suncorpgroup.xml.GINNoticeBoardParser;
import com.myrewards.suncorpgroup.xml.HotOffersParser;
import com.myrewards.suncorpgroup.xml.NoticeboardIdParser;
import com.myrewards.suncorpgroup.xml.ProductAddressesParser;
import com.myrewards.suncorpgroup.xml.ProductDetailsParser;
import com.myrewards.suncorpgroup.xml.UserDetailsParser;

public class GrabItNowService implements CWUNetworkListener {
	private static final int REQUEST_LOGIN = 1;
	private static final int REQUEST_USER_DETAILS = 2;
	@SuppressWarnings("unused")
	private static final int REQUEST_CLIENT_BANNER = 3;
	private static final int REQUEST_CATEGORY_LIST = 4;
	private static final int REQUEST_HOT_OFFERS = 5;
	private static final int REQUEST_SEARCH_PRODUCTS = 6;
	private static final int REQUEST_PRODUCTS_DETAILS = 7;
	private static final int REQUEST_NEAREST_LAT_LON = 8;
	private static final int REQUEST_PRODUCTS_ADDRESSES = 9;
	private static final int REQUEST_REDEEM = 10;
	private static final int REQUEST_HELP_DETAILS = 11;
	private static final int REQUEST_NOTICE_BOARD = 12;
	private static final int REQUEST_DAILY_DEALS = 13;
	private static final int REQUEST_FIRSTTIME_LOGIN = 14;
	private static final int REQUEST_FIRST_TIME_USER_URL = 15;
	private static final int REQUEST_NOTICEBOARD_ID = 16;
	private static final int REQUEST_SEND_A_FRIEND_ID = 17;

	private CWUServiceListener cWUServiceListener;
	private static GrabItNowService myRewardsService;
	private String username;

	public static GrabItNowService getGrabItNowService() {
		if (myRewardsService == null) {
			myRewardsService = new GrabItNowService();
		}
		return myRewardsService;
	}

	public void sendLoginRequest(CWUServiceListener cWUServiceListener,
			String username, String password, String affiliateId) {
		this.cWUServiceListener = cWUServiceListener;
		this.username = username;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.LOGIN_WRAPPER,
				getLoginRequestData(username, password, affiliateId), null,
				this, REQUEST_LOGIN);
	}

	private String getLoginRequestData(String username, String password,
			String subDomainURL) {
		String loginXML = "uname=" + username + "&pwd=" + password + "&sub="
				+ subDomainURL;
		return loginXML;
	}

	private void sendUserDetailsRequest() {
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.USER_DETAILS_WRAPPER + username, null,
				null, this, REQUEST_USER_DETAILS);
	}

	public void sendCategoriesListRequest(CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.CATEGORY_WRAPPER + "&cid="
						+ Utility.user.getClient_id() + "&country="
						+ Utility.user.getCountry(), null, null, this,
				REQUEST_CATEGORY_LIST);
	}

	public void sendHotOffersRequest(CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(ApplicationConstants.HOT_OFFERS_WRAPPER+ Utility.user.getClient_id(), null, null, this, REQUEST_HOT_OFFERS);
	}

	public void sendSearchProductsRequest(
			CWUServiceListener cWUServiceListener, String catID,
			String location, String keyword, int start, int limit) {
		this.cWUServiceListener = cWUServiceListener;
		String queryString = "cid=" + Utility.user.getClient_id();
		if (catID != null) {
			queryString = queryString + "&cat_id=" + catID;
		}
		if (location != null && location.length() > 0) {
			queryString = queryString + "&p=" + location;
		}
		if (keyword != null && keyword.length() > 0) {
			queryString = queryString + "&q=" + keyword;
		}
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.SEARCH_PRODUCTS_WRAPPER + queryString
						+ "&country=" + Utility.user.getCountry() + "&start="
						+ start + "&limit=" + limit, null, null, this,
				REQUEST_SEARCH_PRODUCTS);
	}

	@SuppressWarnings("unused")
	public void sendNearestLatLonRequest(CWUServiceListener cWUServiceListener,
			Double lat, Double lon) {
		this.cWUServiceListener = cWUServiceListener;
		String queryString = "lat=" + lat + "&lng=" + lon + "&cid="
				+ Utility.user.getClient_id() + "&b=0.1" + "&c="
				+ Utility.user.getCountry();
		http: // java-ide-droid.googlecode.com/svn/trunk/ java-ide-droid
			if (Utility.user.getCountry().contains("India")) {
				queryString = "lat=" + lat + "&lng=" + lon + "&cid="+ Utility.user.getClient_id() + "&b=0.041" + "&c="+ Utility.user.getCountry();
			}

		// String queryString = "cid="+Utility.user.getClient_id();
		// queryString=queryString+"&lng="+lon+"&lat="+lat;

		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.NEAREST_LAT_LON_WRAPPER + queryString,
				null, null, this, REQUEST_NEAREST_LAT_LON);
	}

	public void sendProductDetailsRequest(
			CWUServiceListener cWUServiceListener, int productId) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler()
				.sendRequestAsync(
						ApplicationConstants.PRODUCT_DETAILS_WRAPPER + "id="
								+ productId, null, null, this,
						REQUEST_PRODUCTS_DETAILS);
	}

	public void sendProductAddresseRequest(
			CWUServiceListener cWUServiceListener, int productId) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.PRODUCT_ADDRESSES_WRAPPER + productId,
				null, null, this, REQUEST_PRODUCTS_ADDRESSES);
	}

	public void sendRedeemDetailsRequest(CWUServiceListener cWUServiceListener,
			int productId) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.REDEEM_DETAILS_WRAPPER,
				"user_id=" + Utility.user.getId() + "&pid=" + productId
						+ "&cid=" + Utility.user.getClient_id() + "&lat="
						+ Utility.mLat + "&lon=" + Utility.mLng, null, this,
				REQUEST_REDEEM);
	}

	public void sendNoticeBoardRequest(CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(ApplicationConstants.NOTICE_BOARD_WRAPPER_DAILY_SAVERS, null, null, this, REQUEST_NOTICE_BOARD);
		/*CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				// ApplicationConstants.NOTICE_BOARD_WRAPPER+Utility.user.getClient_id(),
				ApplicationConstants.NOTICE_BOARD_WRAPPER, null, null, this,
				REQUEST_NOTICE_BOARD);*/
	}

	/*
	 * public void sendHelpRequest(CWUServiceListener cWUServiceListener) {
	 * this.cWUServiceListener = cWUServiceListener;
	 * CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
	 * ApplicationConstants.HELP_SCREEN_WRAPPER, null, null, this,
	 * REQUEST_HELP_DETAILS); }
	 */

	public void sendDailyDealsRequest(CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				// ApplicationConstants.NOTICE_BOARD_WRAPPER+Utility.user.getClient_id(),
				ApplicationConstants.DAILY_DEALS_WRAPPER + "&country="
						+ Utility.user.getCountry() + "&cid="
						+ Utility.user.getClient_id(), null, null, this,
				REQUEST_DAILY_DEALS);
	}

	public void onRequestCompleted(String response, String errorString,
			int eventType) {
		switch (eventType) {
		case REQUEST_LOGIN:
			if (errorString == null && response != null) {
				try {
					DocumentBuilderFactory factory = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder db = factory.newDocumentBuilder();
					InputSource inStream = new InputSource();
					inStream.setCharacterStream(new StringReader(response));
					Document doc = db.parse(inStream);

					String message = "status";
					NodeList messageId_nl = doc.getElementsByTagName("status");
					for (int i = 0; i < messageId_nl.getLength(); i++) {
						if (messageId_nl.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
							org.w3c.dom.Element nameElement = (org.w3c.dom.Element) messageId_nl
									.item(i);
							message = nameElement.getFirstChild()
									.getNodeValue().trim();
						}
					}
					if (message != null && message.equalsIgnoreCase("SUCCESS")) {
						sendUserDetailsRequest();
					} else
						cWUServiceListener
								.onServiceComplete(message, eventType);

				} catch (Exception e) {
					e.printStackTrace();
					// serviceListener.onServiceComplete(ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_USER_DETAILS:
			if (errorString == null && response != null) {
				try {
					User user = new User();
					new UserDetailsParser().internalXMLParse(response, user);
					cWUServiceListener.onServiceComplete(user, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_CATEGORY_LIST:
			if (errorString == null && response != null) {
				try {
					List<Category> categoryList = new ArrayList<Category>();
					new CategoryListParser().internalXMLParse(response,
							categoryList);
					System.out.println("category list size:   "
							+ categoryList.size());
					cWUServiceListener.onServiceComplete(categoryList,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_HOT_OFFERS:
			if (errorString == null && response != null) {
				try {
					List<Product> productsList = new ArrayList<Product>();
					new HotOffersParser().internalXMLParse(response,
							productsList);
					System.out.println("category list size:   "
							+ productsList.size());
					cWUServiceListener.onServiceComplete(productsList,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_SEARCH_PRODUCTS:
			if (errorString == null && response != null) {
				try {
					List<Product> productsList = new ArrayList<Product>();
					new HotOffersParser().internalXMLParse(response,
							productsList);
					System.out.println("category list size:   "
							+ productsList.size());
					cWUServiceListener.onServiceComplete(productsList,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_NEAREST_LAT_LON:
			if (errorString == null && response != null) {
				try {
					List<Product> productsList = new ArrayList<Product>();
					new HotOffersParser().internalXMLParse(response,
							productsList);
					System.out.println("category list size:   "
							+ productsList.size());
					cWUServiceListener.onServiceComplete(productsList,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_PRODUCTS_DETAILS:
			if (errorString == null && response != null) {
				try {
					ProductDetails productDetails = new ProductDetails();
					new ProductDetailsParser().internalXMLParse(response,
							productDetails);
					cWUServiceListener.onServiceComplete(productDetails,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_PRODUCTS_ADDRESSES:
			if (errorString == null && response != null) {
				try {
					List<ProductAddress> productAddressList = new ArrayList<ProductAddress>();
					new ProductAddressesParser().internalXMLParse(response,
							productAddressList);
					cWUServiceListener.onServiceComplete(productAddressList,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;

		case REQUEST_DAILY_DEALS:
			if (errorString == null && response != null) {
				try {
					Product product1 = new Product();
					new DailyDealsParser().internalXMLParse(response, product1);
					System.out.println("category list size:   " + product1);
					cWUServiceListener.onServiceComplete(product1, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;

		case REQUEST_NOTICE_BOARD:
			if (errorString == null && response != null) {
				try {
					List<NoticeBoard> productsList = new ArrayList<NoticeBoard>();
					new GINNoticeBoardParser().internalXMLParse(response,
							productsList);
					System.out.println("category list size:   "
							+ productsList.size());
					cWUServiceListener.onServiceComplete(productsList,
							eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;

		case REQUEST_REDEEM:
			if (errorString == null && response != null) {
				try {
					cWUServiceListener.onServiceComplete(response, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_HELP_DETAILS:
			if (errorString == null && response != null) {
				try {
					cWUServiceListener.onServiceComplete(response, eventType);
				} catch (Exception e) {
					e.printStackTrace();
					cWUServiceListener
							.onServiceComplete(
									ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL,
									eventType);
				}
			} else if (errorString != null) {
				cWUServiceListener.onServiceComplete(errorString, eventType);
			} else {
				// Log.d(TAG, "unexpected scenario encountered");
			}
			break;
		case REQUEST_FIRSTTIME_LOGIN:
			if (errorString == null && response != null) {

				new FirsttimeLoginParser().IdParser(response);

				cWUServiceListener.onServiceComplete(response, eventType);
			}
			break;
		case REQUEST_FIRST_TIME_USER_URL:
			if (errorString == null && response != null) {
				new FirsttimeLoginParser().IdParser(response);
				cWUServiceListener.onServiceComplete(response, eventType);
			}
			break;
		case REQUEST_NOTICEBOARD_ID:
			if (errorString == null && response != null) {
				List<NoticeId> noticeid = new ArrayList<NoticeId>();

				new NoticeboardIdParser().IdParser(response, noticeid);
				cWUServiceListener.onServiceComplete(noticeid, eventType);

			}
			break;
		case REQUEST_SEND_A_FRIEND_ID:
			if (errorString == null && response != null) {
				if (response != null && response.contains("success")) {
					cWUServiceListener.onServiceComplete(response, eventType);
				} else
					cWUServiceListener.onServiceComplete(response, eventType);
				/*
				 * Document doc=null; DocumentBuilderFactory factory =
				 * DocumentBuilderFactory.newInstance(); try {
				 * 
				 * DocumentBuilder db = factory.newDocumentBuilder();
				 * InputSource inStream = new InputSource();
				 * //response=response.replaceAll("[^\\x20-\\x7e]", "");
				 * inStream.setCharacterStream(new StringReader(response)); doc
				 * = db.parse(inStream); doc.getDocumentElement().normalize();
				 * 
				 * String message = "status"; NodeList messageId_nl =
				 * doc.getElementsByTagName("status"); for (int i = 0; i <
				 * messageId_nl.getLength(); i++) { if
				 * (messageId_nl.item(i).getNodeType() ==
				 * org.w3c.dom.Node.ELEMENT_NODE) { org.w3c.dom.Element
				 * nameElement = (org.w3c.dom.Element) messageId_nl .item(i);
				 * message = nameElement.getFirstChild() .getNodeValue().trim();
				 * } }
				 * 
				 * } catch (Exception e) { e.printStackTrace(); //
				 * serviceListener.onServiceComplete(ApplicationConstants.
				 * UNABLETOESTABLISHCONNECTION_URL); }
				 */
			}
			break;
		}
	}

	public void sendFirstTimeLoginDetails(
			CWUServiceListener cWUServiceListener, String uname,
			String subDomainUrl) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.FIRSTTIME_LOGIN,
				sendFirstLogin(uname, subDomainUrl), null, this,
				REQUEST_FIRSTTIME_LOGIN);

	}

	private String sendFirstLogin(String username, String sub) {
		String str = "uname=" + username + "&sub=" + sub;
		return str;
	}

	public void sendFirstTimeDetails(CWUServiceListener cWUServiceListener,
			String fname, String lname, String passwd, String email,
			String country, String state, int value, int id) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.FIRST_TIME_USER_URL,
				sendFirstTimeUser(fname, lname, passwd, email, country, state,
						value, id), null, this, REQUEST_FIRST_TIME_USER_URL);
	}

	private String sendFirstTimeUser(String firstname, String lastname,
			String password1, String email_id, String countries, String states,
			int i, int idd) {
		String str1 = "fname=" + firstname + "&lname=" + lastname + "&pwd="
				+ password1 + "&email=" + email_id + "&country=" + countries
				+ "&state=" + states + "&newsletter=" + i + "&id=" + idd;
		return str1;
	}

	public void sendNoticeBoardCountIdRequset(
			CWUServiceListener cWUServiceListener) {
		this.cWUServiceListener = cWUServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.NOTICEBOARD_ID_URL, null, null, this,
				REQUEST_NOTICEBOARD_ID);
		// TODO Auto-generated method stub

	}

	public void sendSendAFriendRequest(CWUServiceListener cwuServiceListener,
			int uid, String name, String email) {
		this.cWUServiceListener = cwuServiceListener;
		CWUHttpClient.getWWDispatchHandler().sendRequestAsync(
				ApplicationConstants.SEND_A_FRIEND_ID_URL,
				sendSendAFriendDetails(uid, name, email), null, this,
				REQUEST_SEND_A_FRIEND_ID);
	}

	private String sendSendAFriendDetails(int id, String string, String string2) {
		String sendDetails = "uid=" + id + "&fname=" + string + "&email="
				+ string2;
		return sendDetails;
	}

}
