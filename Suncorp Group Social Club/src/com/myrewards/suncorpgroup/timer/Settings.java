package com.myrewards.suncorpgroup.timer;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.myrewards.suncorpgroup.controller.R;

public class Settings extends PreferenceActivity {
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}
}
