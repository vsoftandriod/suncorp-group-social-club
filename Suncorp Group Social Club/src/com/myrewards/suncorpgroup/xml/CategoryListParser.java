package com.myrewards.suncorpgroup.xml;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.myrewards.suncorpgroup.model.Category;

public class CategoryListParser {
	List<Category> categoryList;
	public void internalXMLParse(String response, List<Category> categoryList) {
		try {
			this.categoryList = categoryList;
		String _node,_element;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
		
			db = factory.newDocumentBuilder();
		
        InputSource inStream = new InputSource();
        inStream.setCharacterStream(new StringReader(response));
        Document doc = db.parse(inStream);  
        doc.getDocumentElement().normalize();
        
        NodeList list2 =    doc.getElementsByTagName("category");
        _node = new String();
        _element = new String();      
        
        for (int i=0;i<list2.getLength();i++){    
        	NodeList childNodes = list2.item(i).getChildNodes();
        	Category category = new Category();
             for(int j=0;j<childNodes.getLength();j++){
            	 Node value=childNodes.item(j).getChildNodes().item(0);
            	 _node=childNodes.item(j).getNodeName();
                 if(value != null){
                	 _element=value.getNodeValue(); 
                	 updateField(_node,_element, category);
                 }
             }
             categoryList.add(category);
        }
        } catch (ParserConfigurationException e) {
			e.printStackTrace();
        } catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void updateField(String node, String element,Category category){
		         
         if(node.equals("id"))
           {
        	 category.setCatID(Integer.parseInt(element));
           }

         else if(node.equals("parent_id"))
           {
             category.setParent_id(Integer.parseInt(element));
             
           }
         else if(node.equals("name"))
           {
        	  category.setName(element);
             
           }
         else if(node.equals("description"))
           {
        	   category.setDescription(element);
           }
          
         else if(node.equals("details"))
           {
            category.setDetails(element);
           }
         else if(node.equals("sort"))
           {
        	   category.setSort(element);
           }
         else if(node.equals("deleted"))
           {
            category.setDeleted(element);
           }
         else if(node.equals("created"))
           {
            category.setCreated(element);
           }
         else  if(node.equals("modified"))
           {
        	   category.setModified(element);
           }
         else if(node.equals("countries"))
         {
          category.setCountries(element);
         }
       else if(node.equals("logo_extension"))
         {
      	   category.setLogo_extension(element);
         }
       else if(node.equals("search_weight"))
         {
          category.setSearch_weight(element);
         }
    }
}
