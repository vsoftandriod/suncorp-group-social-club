package com.myrewards.suncorpgroup.xml;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.myrewards.suncorpgroup.model.Product;


public class DailyDealsParser {
		private Product productDetails;
		public void internalXMLParse(String response, Product productDetails1) {
			try {
				this.productDetails = productDetails1;
			String _node,_element;
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder db;
			
				db = factory.newDocumentBuilder();
			
	        InputSource inStream = new InputSource();
	        inStream.setCharacterStream(new StringReader(response));
	        Document doc = db.parse(inStream);  
	        doc.getDocumentElement().normalize();
	        
	        NodeList list2 =    doc.getElementsByTagName("product");
	        _node = new String();
	        _element = new String();      
	        
	        for (int i=0;i<list2.getLength();i++){    
	        	NodeList childNodes = list2.item(i).getChildNodes();
	             for(int j=0;j<childNodes.getLength();j++){
	            	 Node value=childNodes.item(j).getChildNodes().item(0);
	            	 _node=childNodes.item(j).getNodeName();
	                 if(value != null){
	                	 _element=value.getNodeValue(); 
	                	 updateField(_node,_element, productDetails);
	                 }
	             }
	        }
	        } catch (ParserConfigurationException e) {
				e.printStackTrace();
	        } catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		private void updateField(String node, String element, Product productDetails) {
			if(node.equals("id"))
	        {
				productDetails.setId(Integer.parseInt(element));
	        }
			else if(node.equals("hotoffer_extension"))
	        {
				productDetails.setHotoffer_extension(element);
	        }
			
		}
	}



