package com.myrewards.suncorpgroup.xml;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.myrewards.suncorpgroup.model.ProductDetails;

public class ProductDetailsParser {
	private ProductDetails productDetails;

	public void internalXMLParse(String response, ProductDetails productDetails1) {
		try {
			this.productDetails = productDetails1;
			String _node, _element;

			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder db;

			db = factory.newDocumentBuilder();

			InputSource inStream = new InputSource();
			inStream.setCharacterStream(new StringReader(response));
			Document doc = db.parse(inStream);
			doc.getDocumentElement().normalize();

			NodeList list2 = doc.getElementsByTagName("product");
			_node = new String();
			_element = new String();

			for (int i = 0; i < list2.getLength(); i++) {
				NodeList childNodes = list2.item(i).getChildNodes();
				for (int j = 0; j < childNodes.getLength(); j++) {
					Node value = childNodes.item(j).getChildNodes().item(0);
					_node = childNodes.item(j).getNodeName();
					if (value != null) {
						_element = value.getNodeValue();
						updateField(_node, _element, productDetails);
					}
				}
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void updateField(String node, String element,
			ProductDetails productDetails) {
		if (node.equals("id")) {
			productDetails.setId(Integer.parseInt(element));
		} else if (node.equals("highlight")) {
			productDetails.setHighlight(element);
		} else if (node.equals("name")) {
			productDetails.setName(element);
		}

		if (node.equals("merchant_id")) {
			productDetails.setMerchant_id(Integer.parseInt(element));
		} else if (node.equals("details")) {
			productDetails.setDetails(element);
		} else if (node.equals("text")) {
			productDetails.setText(element);
		}
		if (node.equals("terms_and_conditions")) {
			productDetails.setTerms_and_conditions(element);
		} else if (node.equals("link1")) {
			productDetails.setLink1(element);
		} else if (node.equals("link2")) {
			productDetails.setLink2(element);
		}
		if (node.equals("link2_text")) {
			productDetails.setLink2_text(element);
		} else if (node.equals("image_extension")) {
			productDetails.setImage_extension(element);
		} else if (node.equals("display_image")) {
			productDetails.setDisplay_image(element);
		}
		if (node.equals("mname")) {
			productDetails.setMname(element);
		} else if (node.equals("contact_first_name")) {
			productDetails.setContact_first_name(element);
		} else if (node.equals("contatct_last_name")) {
			productDetails.setContatct_last_name(element);
		}
		if (node.equals("contact_title")) {
			productDetails.setContact_title(element);
		} else if (node.equals("contact_position")) {
			productDetails.setContact_position(element);
		} else if (node.equals("mail_address1")) {
			productDetails.setMail_address1(element);
		}
		if (node.equals("mail_address2")) {
			productDetails.setMail_address2(element);
		} else if (node.equals("mail_suburb")) {
			productDetails.setMail_suburb(element);
		} else if (node.equals("mail_state")) {
			productDetails.setMail_state(element);
		}
		if (node.equals("mail_postcode")) {
			productDetails.setMail_postcode(element);
		} else if (node.equals("mail_country")) {
			productDetails.setMail_country(element);
		} else if (node.equals("email")) {
			productDetails.setEmail(element);
		}
		if (node.equals("phone")) {
			productDetails.setPhone(element);
		} else if (node.equals("mobile")) {
			productDetails.setMobile(element);
		} else if (node.equals("fax")) {
			productDetails.setFax(element);
		}
		if (node.equals("latitude")) {
			productDetails.setLatitude(element);
		} else if (node.equals("longitude")) {
			productDetails.setLongitude(element);
		} else if (node.equals("logo_extension")) {
			productDetails.setLogo_extension(element);
		}
		if (node.equals("m_image_extension")) {
			productDetails.setM_image_extension(element);
		} else if (node.equals("mtext")) {
			productDetails.setMtext(element);
		} else if (node.equals("created")) {
			productDetails.setCreated(element);
		}
		if (node.equals("modified")) {
			productDetails.setModified(element);
		} else if (node.equals("mobile_reward")) {
			productDetails.setMobile_Reward(Integer.parseInt(element));
		} else if (node.equals("quantity")) {
			productDetails.setQuantity(Integer.parseInt(element));
		}
	}
}
